package com.vilynn.web.svg;

import org.apache.batik.dom.svg.SAXSVGDocumentFactory;
import org.junit.Test;
import org.w3c.dom.svg.SVGDocument;

import java.io.IOException;

/**
 * JavaSvgService
 *
 * @author Weilin Wang
 * @since 2020/4/17
 */
public class JavaSvgService {

    @Test
    public void main() throws IOException {
        SVGDocument svgDocument = new SAXSVGDocumentFactory("").createSVGDocument("");
        svgDocument.adoptNode(null);
    }
}
