package com.vilynn.web.vo;

import lombok.Data;

/**
 * CREATE TABLE `mp_miniapp` (
 *   `id` char(24) NOT NULL DEFAULT '' COMMENT '主键',
 *   `miniapp_type` varchar(24) NOT NULL DEFAULT '' COMMENT 'miniapp类型：小程序、公众号等',
 *   `app_id` char(20) NOT NULL,
 *   `app_secret` char(32) NOT NULL,
 *   `app_name` varchar(30) NOT NULL DEFAULT '',
 *   `account_name` varchar(64) NOT NULL DEFAULT '' COMMENT '登录账号名',
 *   `is_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
 *   `is_deleted` bit(1) NOT NULL DEFAULT b'0',
 *   `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 *   `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 *   `is_refresh_token` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否刷新token',
 *   PRIMARY KEY (`id`),
 *   KEY `uk_app_id` (`app_id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='小程序表';
 *
 * @author Weilin Wang
 * @since 2020/3/31
 */
@Data
public class MiniAppDataVo {
    private String id;
    private String miniappType;
    private String appId;
    private String secret;
    private String appName;
    private String accountName;
    private String is_enable;
    private String is_deleted;
    private String is_refresh_token;
}
