package com.vilynn.web.demo.service;

import infra.framework.common.service.BaseService;
import com.vilynn.web.demo.domain.MessageApp;
import com.vilynn.web.demo.domain.MessageAppQuery;

public interface MessageAppService extends BaseService<MessageApp, MessageAppQuery> {

}