package com.vilynn.web.demo.controller;

import com.vilynn.web.demo.domain.MessageApp;
import com.vilynn.web.demo.manager.MessageAppManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequestMapping("/MessageApp")
public class MessageAppController {

	@Autowired
    private MessageAppManager manager;

	@RequestMapping(value = "test", method = RequestMethod.POST)
    public Object test(MessageApp param){
    
    	return null;
    }
    
}