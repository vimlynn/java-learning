package com.vilynn.web.demo.service;

import infra.framework.common.service.BaseService;
import com.vilynn.web.demo.domain.MessageMail;
import com.vilynn.web.demo.domain.MessageMailQuery;

public interface MessageMailService extends BaseService<MessageMail, MessageMailQuery> {

}