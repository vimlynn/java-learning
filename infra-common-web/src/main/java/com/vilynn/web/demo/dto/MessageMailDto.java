package com.vilynn.web.demo.dto;

import lombok.*;

import java.io.Serializable;

import java.util.Date;


/**
 *站内信表?????Dto
*/
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Data
public class MessageMailDto implements Serializable {

		/** 主键 */
	private Long id;
    		/**  */
	private String appId;
    		/** userId */
	private Long userId;
    		/** 模板id */
	private Long templateId;
    		/** 站内信标题 */
	private String title;
    		/** 内容 */
	private String content;
    		/** 是否已读 */
	private Byte readStatus;
    		/** 是否删除 */
	private Byte isDelete;
    		/** 创建时间 */
	private Date createTime;
    		/** 更新时间 */
	private Date updateTime;
    
}