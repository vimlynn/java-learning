package com.vilynn.web.demo.service;

import infra.framework.common.service.BaseService;
import com.vilynn.web.demo.domain.MessageSendMobile;
import com.vilynn.web.demo.domain.MessageSendMobileQuery;

public interface MessageSendMobileService extends BaseService<MessageSendMobile, MessageSendMobileQuery> {

}