package com.vilynn.web.demo.dao;

import com.vilynn.web.demo.domain.MessageMail;
import com.vilynn.web.demo.domain.MessageMailQuery;
import infra.framework.common.dao.BaseDao;

public interface MessageMailDao extends BaseDao<MessageMail, MessageMailQuery> {
}