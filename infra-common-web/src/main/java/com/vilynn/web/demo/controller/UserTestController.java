package com.vilynn.web.demo.controller;

import com.vilynn.web.demo.dao.mapper.UserMapper;
import com.vilynn.web.demo.domain.po.UserPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * UserTestController
 *
 * @author Weilin Wang
 * @since 2020/5/15
 */
@RestController
public class UserTestController {

    @Autowired
    private UserMapper userMapper;

    @RequestMapping("/test")
    public Object test(@RequestParam Integer count, @RequestParam Integer batchSize){
        long start = System.currentTimeMillis();
        int a = 0;
        for (Integer i = 0; i < count;) {
            long start2 = System.currentTimeMillis();
            Collection<UserPO> list = new ArrayList<>();
            for (Integer j = 0; j < batchSize; j++) {
                UserPO userPO = new UserPO();
                userPO.setUserOID(UUID.randomUUID().toString());
                userPO.setIdCard("1234567890");
                userPO.setUserName("weilin.wang");
                userPO.setRealName("王维林");
                userPO.setPassword("123456789");
                userPO.setCellPhone("18626134409");
                userPO.setEmail("5df091de63161b3484b20db8");
                userPO.setIsEmailValidated(false);
                userPO.setIsSmsValidated(false);
                userPO.setFaviconUrl("5df091de63161b3484b20db8");
                userPO.setReferOID("98765432");
                userPO.setCreateTime(new Date());
                userPO.setUpdateTime(new Date());
                userPO.setIsDeleted(false);
                userPO.setLastSignonTime(new Date());
                userPO.setUserSource("5df091de63161b3484b20db8");
                userPO.setSex(0);
                userPO.setCountry("5df091de63161b3484b20db8");
                userPO.setProvince("");
                userPO.setCity("5df091de63161b3484b20db8");
                userPO.setCity_oid(0);
                userPO.setRemark("5df091de63161b3484b20db8");
                userPO.setUserSourceDetail("5df091de63161b3484b20db8");
                userPO.setBindStatus(0);
                userPO.setBirthday(new Date());
                userPO.setAward_point_total(0);
                userPO.setAward_point_expire_total(0);
                userPO.setNickname("5df091de63161b3484b20db8");
                userPO.setMtc_user_id("5df091de63161b3484b20db8");
                userPO.setUser_status("NORMAL");
                userPO.setMerge_user_id("5df091de63161b3484b20db8");
                userPO.setCountry_code("235");
                userPO.setBiz_code("MTL");
                userPO.setDisabled_time(new Date());

                list.add(userPO);
            }
            userMapper.insertBatch(list);
            i += batchSize;
            long end2 = System.currentTimeMillis();
            System.out.println("第"+ ++a +"个批次，one cos time: " + (end2 - start2)/1000);
        }
        long end = System.currentTimeMillis();
        return "cos time: " + (end-start)/1000;
    }

}
