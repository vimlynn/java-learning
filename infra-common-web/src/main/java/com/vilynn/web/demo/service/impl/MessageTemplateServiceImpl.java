package com.vilynn.web.demo.service.impl;

import com.vilynn.web.demo.dao.MessageTemplateDao;
import com.vilynn.web.demo.domain.MessageTemplate;
import com.vilynn.web.demo.domain.MessageTemplateQuery;
import com.vilynn.web.demo.service.MessageTemplateService;
import infra.framework.common.service.impl.AbstractBaseService;
import org.springframework.stereotype.Service;

@Service
public class MessageTemplateServiceImpl extends AbstractBaseService<MessageTemplate, MessageTemplateQuery, MessageTemplateDao> implements MessageTemplateService {

}