package com.vilynn.web.demo.dao;

import com.vilynn.web.demo.domain.MessageSign;
import com.vilynn.web.demo.domain.MessageSignQuery;
import infra.framework.common.dao.BaseDao;

public interface MessageSignDao extends BaseDao<MessageSign, MessageSignQuery> {
}