package com.vilynn.web.demo.dto;

import lombok.*;

import java.io.Serializable;

import java.util.Date;


/**
 *短信签名表?????Dto
*/
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Data
public class MessageSignDto implements Serializable {

		/** 主键 */
	private Long id;
    		/** 签名名称 */
	private String signName;
    		/** 是否删除 */
	private Byte isDelete;
    		/** 创建时间 */
	private Date createTime;
    		/** 更新时间 */
	private Date updateTime;
    
}