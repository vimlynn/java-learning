package com.vilynn.web.demo.controller;

import com.vilynn.web.demo.domain.MessageAccount;
import com.vilynn.web.demo.manager.MessageAccountManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequestMapping("/MessageAccount")
public class MessageAccountController {

	@Autowired
    private MessageAccountManager manager;

	@RequestMapping(value = "test", method = RequestMethod.POST)
    public Object test(MessageAccount param){
    
    	return null;
    }
    
}