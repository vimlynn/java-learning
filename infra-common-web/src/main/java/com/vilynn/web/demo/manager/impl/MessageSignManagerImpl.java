package com.vilynn.web.demo.manager.impl;

import com.vilynn.web.demo.service.MessageSignService;
import com.vilynn.web.demo.manager.MessageSignManager;
import infra.framework.common.manager.impl.AbstractBaseManager;
import org.springframework.stereotype.Service;

@Service
public class MessageSignManagerImpl extends AbstractBaseManager<MessageSignService> implements MessageSignManager {
}