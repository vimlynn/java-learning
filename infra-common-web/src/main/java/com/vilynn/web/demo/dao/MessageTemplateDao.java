package com.vilynn.web.demo.dao;

import com.vilynn.web.demo.domain.MessageTemplate;
import com.vilynn.web.demo.domain.MessageTemplateQuery;
import infra.framework.common.dao.BaseDao;

public interface MessageTemplateDao extends BaseDao<MessageTemplate, MessageTemplateQuery> {
}