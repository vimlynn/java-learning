package com.vilynn.web.demo.controller;

import com.vilynn.web.demo.domain.MessageTemplate;
import com.vilynn.web.demo.manager.MessageTemplateManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequestMapping("/MessageTemplate")
public class MessageTemplateController {

	@Autowired
    private MessageTemplateManager manager;

	@RequestMapping(value = "test", method = RequestMethod.POST)
    public Object test(MessageTemplate param){
    
    	return null;
    }
    
}