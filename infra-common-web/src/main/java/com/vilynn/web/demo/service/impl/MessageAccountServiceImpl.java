package com.vilynn.web.demo.service.impl;

import com.vilynn.web.demo.dao.MessageAccountDao;
import com.vilynn.web.demo.domain.MessageAccount;
import com.vilynn.web.demo.domain.MessageAccountQuery;
import com.vilynn.web.demo.service.MessageAccountService;
import infra.framework.common.service.impl.AbstractBaseService;
import org.springframework.stereotype.Service;

@Service
public class MessageAccountServiceImpl extends AbstractBaseService<MessageAccount, MessageAccountQuery, MessageAccountDao> implements MessageAccountService {

}