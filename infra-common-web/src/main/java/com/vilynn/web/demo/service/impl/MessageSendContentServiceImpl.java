package com.vilynn.web.demo.service.impl;

import com.vilynn.web.demo.dao.MessageSendContentDao;
import com.vilynn.web.demo.domain.MessageSendContent;
import com.vilynn.web.demo.domain.MessageSendContentQuery;
import com.vilynn.web.demo.service.MessageSendContentService;
import infra.framework.common.service.impl.AbstractBaseService;
import org.springframework.stereotype.Service;

@Service
public class MessageSendContentServiceImpl extends AbstractBaseService<MessageSendContent, MessageSendContentQuery, MessageSendContentDao> implements MessageSendContentService {

}