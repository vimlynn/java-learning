package com.vilynn.web.demo.controller;

import com.vilynn.web.demo.domain.MessageSign;
import com.vilynn.web.demo.manager.MessageSignManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequestMapping("/MessageSign")
public class MessageSignController {

	@Autowired
    private MessageSignManager manager;

	@RequestMapping(value = "test", method = RequestMethod.POST)
    public Object test(MessageSign param){
    
    	return null;
    }
    
}