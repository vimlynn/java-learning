package com.vilynn.web.demo.manager.impl;

import com.vilynn.web.demo.service.MessageAppService;
import com.vilynn.web.demo.manager.MessageAppManager;
import infra.framework.common.manager.impl.AbstractBaseManager;
import org.springframework.stereotype.Service;

@Service
public class MessageAppManagerImpl extends AbstractBaseManager<MessageAppService> implements MessageAppManager {
}