package com.vilynn.web.demo.dao;

import com.vilynn.web.demo.domain.MessageAccount;
import com.vilynn.web.demo.domain.MessageAccountQuery;
import infra.framework.common.dao.BaseDao;

public interface MessageAccountDao extends BaseDao<MessageAccount, MessageAccountQuery> {
}