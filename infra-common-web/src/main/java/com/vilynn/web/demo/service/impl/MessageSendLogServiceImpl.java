package com.vilynn.web.demo.service.impl;

import com.vilynn.web.demo.dao.MessageSendLogDao;
import com.vilynn.web.demo.domain.MessageSendLog;
import com.vilynn.web.demo.domain.MessageSendLogQuery;
import com.vilynn.web.demo.service.MessageSendLogService;
import infra.framework.common.service.impl.AbstractBaseService;
import org.springframework.stereotype.Service;

@Service
public class MessageSendLogServiceImpl extends AbstractBaseService<MessageSendLog, MessageSendLogQuery, MessageSendLogDao> implements MessageSendLogService {

}