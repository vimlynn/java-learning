package com.vilynn.web.demo.service.impl;

import com.vilynn.web.demo.dao.MessageAppDao;
import com.vilynn.web.demo.domain.MessageApp;
import com.vilynn.web.demo.domain.MessageAppQuery;
import com.vilynn.web.demo.service.MessageAppService;
import infra.framework.common.service.impl.AbstractBaseService;
import org.springframework.stereotype.Service;

@Service
public class MessageAppServiceImpl extends AbstractBaseService<MessageApp, MessageAppQuery, MessageAppDao> implements MessageAppService {

}