package com.vilynn.web.demo.domain;

import infra.framework.common.dto.BaseQuery;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Data
public class MessageAccountQuery extends BaseQuery {

		/** 主键 */
	private Long id;
    		/** 渠道code码 */
	private String channelCode;
    		/** 渠道类型 */
	private Byte templateType;
    		/** 0-国内，1-国外 */
	private Byte inHome;
    		/** 签名ID */
	private Long signId;
    		/** 签名名称 */
	private String signName;
    		/** 用户名 */
	private String userName;
    		/** 密码 */
	private String password;
    		/** 请求类型:POST/GET */
	private String postType;
    		/** 请求地址 */
	private String baseUrl;
    		/** 属性值 */
	private String attribute;
    		/** 备注 */
	private String remark;
    		/** 是否删除 */
	private Byte isDelete;
    		/** 创建时间 */
	private Date createTime;
    		/** 更新时间 */
	private Date updateTime;
    
}