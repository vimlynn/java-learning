package com.vilynn.web.demo.controller;

import com.vilynn.web.demo.domain.MessageSendContent;
import com.vilynn.web.demo.manager.MessageSendContentManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/MessageSendContent")
public class MessageSendContentController {

	@Autowired
    private MessageSendContentManager manager;

    @RequestMapping(value = "test", method = RequestMethod.GET)
    public Object test(@RequestParam int num){

        List<MessageSendContent> list = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            MessageSendContent content = new MessageSendContent();
            list.add(content);

            content.setAppId("fzd");
            content.setChannelCode("WW");
            content.setCreateTime(new Date());
            content.setContent("ww萨达达阿大使 阿斯顿啊阿斯顿大厦打萨达萨达到萨达我企鹅温柔额萨达萨达第三方大厦史蒂夫");
            content.setSendType(Byte.valueOf("12"));
            content.setStatus("ACCEPT");
            content.setTemplateId(1L);
            content.setTemplateType(new Byte("2"));
            content.setSignId(1L);
            content.setIsDelete(new Byte("0"));
        }

        int m = 0;
        long start1 = System.currentTimeMillis();
        for (int i = 0; i < list.size(); i++) {
            m += manager.insert(list.get(i));
        }
        long end1 = System.currentTimeMillis();
        long time1 = (end1 - start1);

        long start2 = System.currentTimeMillis();
        int n = manager.insertBatch(list);
        long end2 = System.currentTimeMillis();
        long time2 = (end2 - start2);

        return String.format("for循环：%d,耗时%d s;\n批量：%d,耗时%d s", m, time1, n, time2);
    }
    
}