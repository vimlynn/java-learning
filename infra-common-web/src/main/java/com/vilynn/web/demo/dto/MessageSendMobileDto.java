package com.vilynn.web.demo.dto;

import lombok.*;

import java.io.Serializable;

import java.util.Date;


/**
 *消息发送手机号表?????Dto
*/
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Data
public class MessageSendMobileDto implements Serializable {

		/** 主键 */
	private Long id;
    		/** 发送内容id */
	private Long messageSendContentId;
    		/** 手机号 */
	private String mobile;
    		/** 是否删除 */
	private Byte isDelete;
    		/** 创建时间 */
	private Date createTime;
    		/** 更新时间 */
	private Date updateTime;
    
}