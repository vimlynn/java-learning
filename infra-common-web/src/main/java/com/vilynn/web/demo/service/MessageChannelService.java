package com.vilynn.web.demo.service;

import infra.framework.common.service.BaseService;
import com.vilynn.web.demo.domain.MessageChannel;
import com.vilynn.web.demo.domain.MessageChannelQuery;

public interface MessageChannelService extends BaseService<MessageChannel, MessageChannelQuery> {

}