package com.vilynn.web.demo.service.impl;

import com.vilynn.web.demo.dao.MessageChannelDao;
import com.vilynn.web.demo.domain.MessageChannel;
import com.vilynn.web.demo.domain.MessageChannelQuery;
import com.vilynn.web.demo.service.MessageChannelService;
import infra.framework.common.service.impl.AbstractBaseService;
import org.springframework.stereotype.Service;

@Service
public class MessageChannelServiceImpl extends AbstractBaseService<MessageChannel, MessageChannelQuery, MessageChannelDao> implements MessageChannelService {

}