package com.vilynn.web.demo.domain;

import infra.framework.common.dto.BaseQuery;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Data
public class MessageChannelQuery extends BaseQuery {

		/** 主键 */
	private Long id;
    		/** 渠道名称 */
	private String channelName;
    		/** 渠道code码 */
	private String channelCode;
    		/** 是否删除 */
	private Byte isDelete;
    		/** 创建时间 */
	private Date createTime;
    		/** 更新时间 */
	private Date updateTime;
    
}