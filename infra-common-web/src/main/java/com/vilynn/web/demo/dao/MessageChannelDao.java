package com.vilynn.web.demo.dao;

import com.vilynn.web.demo.domain.MessageChannel;
import com.vilynn.web.demo.domain.MessageChannelQuery;
import infra.framework.common.dao.BaseDao;

public interface MessageChannelDao extends BaseDao<MessageChannel, MessageChannelQuery> {
}