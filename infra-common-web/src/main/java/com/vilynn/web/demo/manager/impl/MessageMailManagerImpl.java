package com.vilynn.web.demo.manager.impl;

import com.vilynn.web.demo.service.MessageMailService;
import com.vilynn.web.demo.manager.MessageMailManager;
import infra.framework.common.manager.impl.AbstractBaseManager;
import org.springframework.stereotype.Service;

@Service
public class MessageMailManagerImpl extends AbstractBaseManager<MessageMailService> implements MessageMailManager {
}