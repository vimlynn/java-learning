package com.vilynn.web.demo.service;

import infra.framework.common.service.BaseService;
import com.vilynn.web.demo.domain.MessageTemplate;
import com.vilynn.web.demo.domain.MessageTemplateQuery;

public interface MessageTemplateService extends BaseService<MessageTemplate, MessageTemplateQuery> {

}