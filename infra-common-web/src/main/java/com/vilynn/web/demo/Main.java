package com.vilynn.web.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by wangweilin on 2018/11/28.
 */
@SpringBootApplication
@MapperScan("com.xiamoan.demo.dao")
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
