package com.vilynn.web.demo.manager.impl;

import com.vilynn.web.demo.service.MessageSendMobileService;
import com.vilynn.web.demo.manager.MessageSendMobileManager;
import infra.framework.common.manager.impl.AbstractBaseManager;
import org.springframework.stereotype.Service;

@Service
public class MessageSendMobileManagerImpl extends AbstractBaseManager<MessageSendMobileService> implements MessageSendMobileManager {
}