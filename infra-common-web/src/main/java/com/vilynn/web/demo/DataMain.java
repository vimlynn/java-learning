package com.vilynn.web.demo;

import com.alibaba.fastjson.JSON;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

/**
 * Created by wangweilin on 2019/1/10.
 */
public class DataMain {
    private String h;
    private static final RestTemplate rest = new RestTemplate();
    private static final String url = "http://localhost:8080/message-app/message/sendMutilTemplateMessage";
    public static void send() {
        String s = "{\n" +
                "\t\"messageParams\": [{\n" +
                "\t\t\t\"orgCode\": \"JG01\",\n" +
                "\t\t\t\"appId\": \"fzd\",\n" +
                "\t\t\t\"userIds\": [\n" +
                "\t\t\t\t18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408\n" +
                "\t\t\t],\n" +
                "\t\t\t\"templateId\": 221\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"orgCode\": \"JG02\",\n" +
                "\t\t\t\"templateId\": 101202,\n" +
                "\t\t\t\"appId\": \"yjqb\",\n" +
                "\t\t\t\"params\": [{}],\n" +
                "\t\t\t\"mobileNums\": [\n" +
                "\t\t\t\t18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408,18626144407,18626144408\n" +
                "\t\t\t]\n" +
                "\t\t}\n" +
                "\t]\n" +
                "}";

        Object o = rest.postForObject(url, JSON.parseObject(s), HashMap.class);
        System.out.println(o.toString());
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                while (true) {
                    send();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    public static void s(){
        DataMain m;
        int i = 0;
        if(i == 1){
            System.out.println("1");
        }else if((m = new DataMain()) != null){
            System.out.println("2");
        }else if(m.h == null){
            System.out.println("3");
        }
    }
}
