package com.vilynn.web.demo.dao;

import com.vilynn.web.demo.domain.MessageSendContent;
import com.vilynn.web.demo.domain.MessageSendContentQuery;
import infra.framework.common.dao.BaseDao;

public interface MessageSendContentDao extends BaseDao<MessageSendContent, MessageSendContentQuery> {
}