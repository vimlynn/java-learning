package com.vilynn.web.demo.manager;

import com.vilynn.web.demo.domain.MessageSendContent;
import infra.framework.common.manager.BaseManager;

import java.util.List;

public interface MessageSendContentManager extends BaseManager {

    int insert(MessageSendContent messageSendContent);

    int insertBatch(List<MessageSendContent> list);
}