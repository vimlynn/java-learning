package com.vilynn.web.demo.manager.impl;

import com.vilynn.web.demo.service.MessageTemplateService;
import com.vilynn.web.demo.manager.MessageTemplateManager;
import infra.framework.common.manager.impl.AbstractBaseManager;
import org.springframework.stereotype.Service;

@Service
public class MessageTemplateManagerImpl extends AbstractBaseManager<MessageTemplateService> implements MessageTemplateManager {
}