package com.vilynn.web.demo.service;

import infra.framework.common.service.BaseService;
import com.vilynn.web.demo.domain.MessageSendContent;
import com.vilynn.web.demo.domain.MessageSendContentQuery;

public interface MessageSendContentService extends BaseService<MessageSendContent, MessageSendContentQuery> {

}