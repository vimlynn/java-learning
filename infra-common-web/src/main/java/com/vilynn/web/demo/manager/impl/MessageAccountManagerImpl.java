package com.vilynn.web.demo.manager.impl;

import com.vilynn.web.demo.service.MessageAccountService;
import com.vilynn.web.demo.manager.MessageAccountManager;
import infra.framework.common.manager.impl.AbstractBaseManager;
import org.springframework.stereotype.Service;

@Service
public class MessageAccountManagerImpl extends AbstractBaseManager<MessageAccountService> implements MessageAccountManager {


}