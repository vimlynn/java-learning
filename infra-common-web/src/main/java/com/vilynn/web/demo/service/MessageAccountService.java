package com.vilynn.web.demo.service;

import infra.framework.common.service.BaseService;
import com.vilynn.web.demo.domain.MessageAccount;
import com.vilynn.web.demo.domain.MessageAccountQuery;

public interface MessageAccountService extends BaseService<MessageAccount, MessageAccountQuery> {

}