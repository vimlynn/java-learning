package com.vilynn.web.demo.dao;

import com.vilynn.web.demo.domain.MessageSendLog;
import com.vilynn.web.demo.domain.MessageSendLogQuery;
import infra.framework.common.dao.BaseDao;

public interface MessageSendLogDao extends BaseDao<MessageSendLog, MessageSendLogQuery> {
}