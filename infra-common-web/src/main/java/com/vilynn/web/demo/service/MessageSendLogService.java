package com.vilynn.web.demo.service;

import infra.framework.common.service.BaseService;
import com.vilynn.web.demo.domain.MessageSendLog;
import com.vilynn.web.demo.domain.MessageSendLogQuery;

public interface MessageSendLogService extends BaseService<MessageSendLog, MessageSendLogQuery> {

}