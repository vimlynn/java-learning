package com.vilynn.web.demo.manager.impl;

import com.vilynn.web.demo.service.MessageSendLogService;
import com.vilynn.web.demo.manager.MessageSendLogManager;
import infra.framework.common.manager.impl.AbstractBaseManager;
import org.springframework.stereotype.Service;

@Service
public class MessageSendLogManagerImpl extends AbstractBaseManager<MessageSendLogService> implements MessageSendLogManager {
}