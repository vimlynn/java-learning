package com.vilynn.web.demo.controller;

import com.vilynn.web.demo.domain.MessageChannel;
import com.vilynn.web.demo.manager.MessageChannelManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequestMapping("/MessageChannel")
public class MessageChannelController {

	@Autowired
    private MessageChannelManager manager;

	@RequestMapping(value = "test", method = RequestMethod.POST)
    public Object test(MessageChannel param){
    
    	return null;
    }
    
}