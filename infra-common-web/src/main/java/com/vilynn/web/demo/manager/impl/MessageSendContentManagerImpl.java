package com.vilynn.web.demo.manager.impl;

import com.vilynn.web.demo.domain.MessageSendContent;
import com.vilynn.web.demo.service.MessageSendContentService;
import com.vilynn.web.demo.manager.MessageSendContentManager;
import infra.framework.common.manager.impl.AbstractBaseManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageSendContentManagerImpl extends AbstractBaseManager<MessageSendContentService> implements MessageSendContentManager {

    @Autowired
    private MessageSendContentService service;

    @Override
    public int insert(MessageSendContent messageSendContent) {
        return getService().saveSelective(messageSendContent);
    }

    @Override
    public int insertBatch(List<MessageSendContent> list) {
        return service.saveBatch(list);
    }
}