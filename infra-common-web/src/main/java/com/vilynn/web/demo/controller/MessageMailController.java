package com.vilynn.web.demo.controller;

import com.vilynn.web.demo.domain.MessageMail;
import com.vilynn.web.demo.manager.MessageMailManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequestMapping("/MessageMail")
public class MessageMailController {

	@Autowired
    private MessageMailManager manager;

	@RequestMapping(value = "test", method = RequestMethod.POST)
    public Object test(MessageMail param){
    
    	return null;
    }
    
}