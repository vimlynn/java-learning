package com.vilynn.web.demo.domain.po;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * tm_user
 * @author Administrator
 * @date 2020-05-15 17:57:07
 */
@Data
@ToString(callSuper = true)
//@EqualsAndHashCode(callSuper = true)
public class UserPO extends Object {
    /**
     */
    private String userOID;

    /**
     */
    private String idCard;

    /**
     */
    private String userName;

    /**
     */
    private String realName;

    /**
     * 加密过的密码
     */
    private String password;

    /**
     */
    private String cellPhone;

    /**
     */
    private String email;

    /**
     */
    private Boolean isEmailValidated;

    /**
     */
    private Boolean isSmsValidated;

    /**
     */
    private String faviconUrl;

    /**
     */
    private String referOID;

    /**
     */
    private Date createTime;

    /**
     */
    private Date updateTime;

    /**
     */
    private Boolean isDeleted;

    /**
     */
    private Date lastSignonTime;

    /**
     */
    private String userSource;

    /**
     */
    private Integer sex;

    /**
     */
    private String country;

    /**
     */
    private String province;

    /**
     */
    private String city;

    /**
     * 城市ID
     */
    private Integer city_oid;

    /**
     */
    private String remark;

    /**
     */
    private String userSourceDetail;

    /**
     * 微信号是否绑定过手机 0:未绑定 1：已绑定
     */
    private Integer bindStatus;

    /**
     * 生日
     */
    private Date birthday;

    /**
     * 总积分数
     */
    private Integer award_point_total;

    /**
     * 总待失效积分数
     */
    private Integer award_point_expire_total;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 关联用户中心ID
     */
    private String mtc_user_id;

    /**
     * 状态
     */
    private String user_status;

    /**
     * 合并到的卖家Id
     */
    private String merge_user_id;

    /**
     */
    private String country_code;

    /**
     * 业务端编码
     */
    private String biz_code;

    /**
     * 注销时间（当user_status是DISABLED的时候才有意义）
     */
    private Date disabled_time;
}