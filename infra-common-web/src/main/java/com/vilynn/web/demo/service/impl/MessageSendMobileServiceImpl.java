package com.vilynn.web.demo.service.impl;

import com.vilynn.web.demo.dao.MessageSendMobileDao;
import com.vilynn.web.demo.domain.MessageSendMobile;
import com.vilynn.web.demo.domain.MessageSendMobileQuery;
import com.vilynn.web.demo.service.MessageSendMobileService;
import infra.framework.common.service.impl.AbstractBaseService;
import org.springframework.stereotype.Service;

@Service
public class MessageSendMobileServiceImpl extends AbstractBaseService<MessageSendMobile, MessageSendMobileQuery, MessageSendMobileDao> implements MessageSendMobileService {

}