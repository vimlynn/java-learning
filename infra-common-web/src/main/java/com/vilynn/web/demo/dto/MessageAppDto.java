package com.vilynn.web.demo.dto;

import lombok.*;

import java.io.Serializable;

import java.util.Date;


/**
 *系统表?????Dto
*/
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Data
public class MessageAppDto implements Serializable {

		/** 主键 */
	private Long id;
    		/** 项目id标示 */
	private String appId;
    		/** 项目名称，例如：分子贷 */
	private String appName;
    		/** App唯一标识，MD5值 */
	private String appKey;
    		/** 是否删除 */
	private Byte isDelete;
    		/** 创建时间 */
	private Date createTime;
    		/** 更新时间 */
	private Date updateTime;
    
}