package com.vilynn.web.demo.dao.mapper;

import com.vilynn.web.demo.domain.po.UserPO;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;

/**
 * UserMapper
 *
 * @author Weilin Wang
 * @since 2020/5/15
 */
public interface UserMapper {
    int insert(@Param("entity") UserPO userPO);
    int insertBatch(@Param("entities") Collection<UserPO> userPOs);
}
