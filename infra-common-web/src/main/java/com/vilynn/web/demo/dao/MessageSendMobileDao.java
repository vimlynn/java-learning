package com.vilynn.web.demo.dao;

import com.vilynn.web.demo.domain.MessageSendMobile;
import com.vilynn.web.demo.domain.MessageSendMobileQuery;
import infra.framework.common.dao.BaseDao;

public interface MessageSendMobileDao extends BaseDao<MessageSendMobile, MessageSendMobileQuery> {
}