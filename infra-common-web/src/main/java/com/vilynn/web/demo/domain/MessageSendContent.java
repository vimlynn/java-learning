package com.vilynn.web.demo.domain;

import infra.framework.common.dto.BaseDomain;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Data
public class MessageSendContent extends BaseDomain {

		/** 主键 */
	private Long id;
    		/**  */
	private String appId;
    		/** 签名id */
	private Long signId;
    		/** 模板id */
	private Long templateId;
    		/** 渠道code码 */
	private String channelCode;
    		/** 模版类型：1-通知、2-催收、3-审核、4-营销 */
	private Byte templateType;
    		/** 发送类型：1-sms,2-inMail */
	private Byte sendType;
    		/** 发送内容 */
	private String content;
    		/** 发送状态 */
	private String status;
    		/** 是否删除 */
	private Byte isDelete;
    		/** 创建时间 */
	private Date createTime;
    		/** 更新时间 */
	private Date updateTime;
    		/** 版本号 */
	private Integer version;
    
}