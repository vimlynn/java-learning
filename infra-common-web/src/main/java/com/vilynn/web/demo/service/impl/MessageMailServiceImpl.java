package com.vilynn.web.demo.service.impl;

import com.vilynn.web.demo.dao.MessageMailDao;
import com.vilynn.web.demo.domain.MessageMail;
import com.vilynn.web.demo.domain.MessageMailQuery;
import com.vilynn.web.demo.service.MessageMailService;
import infra.framework.common.service.impl.AbstractBaseService;
import org.springframework.stereotype.Service;

@Service
public class MessageMailServiceImpl extends AbstractBaseService<MessageMail, MessageMailQuery, MessageMailDao> implements MessageMailService {

}