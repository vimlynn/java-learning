package com.vilynn.web.demo.service.impl;

import com.vilynn.web.demo.dao.MessageSignDao;
import com.vilynn.web.demo.domain.MessageSign;
import com.vilynn.web.demo.domain.MessageSignQuery;
import com.vilynn.web.demo.service.MessageSignService;
import infra.framework.common.service.impl.AbstractBaseService;
import org.springframework.stereotype.Service;

@Service
public class MessageSignServiceImpl extends AbstractBaseService<MessageSign, MessageSignQuery, MessageSignDao> implements MessageSignService {

}