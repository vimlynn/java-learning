package com.vilynn.web.demo.controller;

import com.vilynn.web.demo.domain.MessageSendMobile;
import com.vilynn.web.demo.manager.MessageSendMobileManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequestMapping("/MessageSendMobile")
public class MessageSendMobileController {

	@Autowired
    private MessageSendMobileManager manager;

	@RequestMapping(value = "test", method = RequestMethod.POST)
    public Object test(MessageSendMobile param){
    
    	return null;
    }
    
}