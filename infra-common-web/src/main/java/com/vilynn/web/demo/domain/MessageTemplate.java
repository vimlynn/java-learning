package com.vilynn.web.demo.domain;

import infra.framework.common.dto.BaseDomain;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Data
public class MessageTemplate extends BaseDomain {

		/** 主键 */
	private Long id;
    		/** 模版ID，非主键，不可变更 */
	private Long templateId;
    		/**  */
	private String appId;
    		/** 签名id */
	private Long signId;
    		/** 机构code */
	private String orgCode;
    		/** 模版类型：1-通知、2-催收、3-审核、4-营销 */
	private Byte templateType;
    		/** 发送类型：1-sms,2-inMail */
	private String sendType;
    		/** 模板标题 */
	private String title;
    		/** 模板内容 */
	private String content;
    		/** 是否删除 */
	private Byte isDelete;
    		/** 创建时间 */
	private Date createTime;
    		/** 更新时间 */
	private Date updateTime;
    		/** 版本号 */
	private Integer version;
    
}