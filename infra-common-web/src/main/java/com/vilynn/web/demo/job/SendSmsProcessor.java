package com.vilynn.web.demo.job;

import com.vilynn.web.demo.domain.MessageSendContent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;

/**
 * Created by wangweilin on 2018/11/27.
 */
//@Component
@Slf4j
public class SendSmsProcessor implements ItemProcessor<MessageSendContent, MessageSendContent> {
    @Override
    public MessageSendContent process(MessageSendContent item) throws Exception {
        log.info("发送短信成功");
        return null;
    }
}
