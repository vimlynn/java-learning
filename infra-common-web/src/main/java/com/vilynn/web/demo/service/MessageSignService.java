package com.vilynn.web.demo.service;

import infra.framework.common.service.BaseService;
import com.vilynn.web.demo.domain.MessageSign;
import com.vilynn.web.demo.domain.MessageSignQuery;

public interface MessageSignService extends BaseService<MessageSign, MessageSignQuery> {

}