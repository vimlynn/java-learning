package com.vilynn.web.demo.dao;

import com.vilynn.web.demo.domain.MessageApp;
import com.vilynn.web.demo.domain.MessageAppQuery;
import infra.framework.common.dao.BaseDao;

public interface MessageAppDao extends BaseDao<MessageApp, MessageAppQuery> {
}