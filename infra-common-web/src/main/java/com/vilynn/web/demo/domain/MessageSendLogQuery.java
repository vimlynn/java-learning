package com.vilynn.web.demo.domain;

import infra.framework.common.dto.BaseQuery;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@Data
public class MessageSendLogQuery extends BaseQuery {

		/** 主键 */
	private Long id;
    		/** 发送商返回结果id */
	private String msgId;
    		/** 发送内容id */
	private Long messageSendContentId;
    		/** 手机号 */
	private String mobile;
    		/** 模板id */
	private Long templateId;
    		/** 发送账户id */
	private Long sendAccountId;
    		/** 渠道code码 */
	private String channelCode;
    		/** 供应商发送结果状态 */
	private String status;
    		/** 是否删除 */
	private Byte isDelete;
    		/** 创建时间 */
	private Date createTime;
    		/** 更新时间 */
	private Date updateTime;
    		/** 版本号 */
	private Integer version;
    
}