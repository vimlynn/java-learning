package com.vilynn.web.demo.manager.impl;

import com.vilynn.web.demo.service.MessageChannelService;
import com.vilynn.web.demo.manager.MessageChannelManager;
import infra.framework.common.manager.impl.AbstractBaseManager;
import org.springframework.stereotype.Service;

@Service
public class MessageChannelManagerImpl extends AbstractBaseManager<MessageChannelService> implements MessageChannelManager {
}