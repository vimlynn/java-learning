package infra.framework.common.manager.impl;

import infra.framework.common.manager.BaseManager;
import infra.framework.common.service.BaseService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by wangweilin on 2018/10/29.
 */
@Data
public class AbstractBaseManager<S extends BaseService> implements BaseManager {

    @Autowired
    private S service;

}
