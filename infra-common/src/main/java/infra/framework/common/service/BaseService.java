package infra.framework.common.service;

import java.util.List;

/**
 * Created by wangweilin on 2018/10/29.
 */
public interface BaseService<T, K> {

    T getByPrimaryKey(Long id);
    T get(T record);

    int deleteByPrimaryKey(Long id);
    int del(T record);
    int delByCondition(T record);

    List<T> findByCondition(K query);
    int findTotalCount(K query);

    int save(T record);
    int saveSelective(T record);
    int saveBatch(List<T> record);

    int updateByPrimaryKeySelective(T record);
    int update(T record);
}
