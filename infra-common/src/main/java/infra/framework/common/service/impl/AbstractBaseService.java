package infra.framework.common.service.impl;

import infra.framework.common.dao.BaseDao;
import infra.framework.common.service.BaseService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by wangweilin on 2018/10/29.
 */
//@Data
public class AbstractBaseService<T,K,D extends BaseDao<T,K>> implements BaseService<T,K> {

    @Autowired
    private D dao;


    public T getByPrimaryKey(Long id) {
        return getDao().getByPrimaryKey(id);
    }

    public T get(T record) {
        return getDao().get(record);
    }

    public int deleteByPrimaryKey(Long id) {
        return getDao().deleteByPrimaryKey(id);
    }

    public int del(T record) {
        return getDao().del(record);
    }

    public int delByCondition(T record) {
        return getDao().delByCondition(record);
    }

    public List<T> findByCondition(K query) {
        return getDao().findByCondition(query);
    }

    public int findTotalCount(K query) {
        return getDao().findTotalCount(query);
    }

    public int save(T record) {
        return getDao().save(record);
    }

    public int saveSelective(T record) {
        return getDao().saveSelective(record);
    }

    public int saveBatch(List<T> record) {
        return getDao().saveBatch(record);
    }

    public int updateByPrimaryKeySelective(T record) {
        return getDao().updateByPrimaryKeySelective(record);
    }

    public int update(T record) {
        return getDao().update(record);
    }

    public D getDao() {
        return dao;
    }

    @Autowired
    public void setDao(D dao) {
        this.dao = dao;
    }
}
