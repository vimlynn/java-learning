package infra.framework.common.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by wangweilin on 2018/10/29.
 */
public interface BaseDao<T, K> {

    T getByPrimaryKey(Long id);
    T get(@Param("entity") T record);

    int deleteByPrimaryKey(Long id);
    int del(@Param("entity") T record);
    int delByCondition(@Param("entity") T record);

    List<T> findByCondition(@Param("query") K query);
    int findTotalCount(@Param("query") K query);

    int save(@Param("entity") T record);
    int saveSelective(@Param("entity") T record);
    int saveBatch(@Param("entities") List<T> record);

    int updateByPrimaryKeySelective(@Param("entity") T record);
    int update(@Param("entity") T record);
}
