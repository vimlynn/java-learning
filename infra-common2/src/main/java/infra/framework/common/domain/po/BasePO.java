package infra.framework.common.domain.po;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by wangweilin on 2018/11/2.
 */
@Data
public class BasePO implements Serializable {

    private Long id;

    private Boolean isDeleted;

    private Date createTime;

    private Date updateTime;
}
