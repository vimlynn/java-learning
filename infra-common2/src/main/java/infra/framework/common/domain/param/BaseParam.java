package infra.framework.common.domain.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by wangweilin on 2018/11/2.
 */
@Data
public class BaseParam implements Serializable {

    // 页码
    private int pageNo = 1;

    // 每页数量
    private int pageSize = 20;

    // 多字段排序
    private List<SortField> sortFields;

    public void addSort(String fieldName, SortMethod method) {
        SortField sortField = new SortField(fieldName, method);
        this.addSort(sortField);
    }

    public void addSort(SortField sortField) {
        this.sortFields = Optional.ofNullable(sortFields).orElse(new ArrayList<>());
        this.sortFields.add(sortField);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SortField {

        private String fieldName;

        private SortMethod method;
    }

    public enum SortMethod {
        DESC, ASC;
    }

}
