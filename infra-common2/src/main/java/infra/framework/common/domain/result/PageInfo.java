package infra.framework.common.domain.result;

import infra.framework.common.domain.info.BaseInfo;
import com.github.pagehelper.Page;
import lombok.Data;

import java.util.List;

/**
 * PageInfo
 *
 * @author Weilin Wang
 * @since 2020/4/24
 */
@Data
public class PageInfo<T> {
    private long totalCount;
    private int pageNo;
    private int pageSize;
    private List<T> data;

    public static <I extends BaseInfo, R> PageInfo<I> newInstance(Page<R> page, List<I> list) {
        PageInfo pageInfo = new PageInfo();
        pageInfo.setTotalCount(page.getTotal());
        pageInfo.setPageNo(page.getPageNum());
        pageInfo.setPageSize(page.getPageSize());
        pageInfo.setData(list);
        return pageInfo;
    }
}
