package infra.framework.common.domain.result;

import lombok.Data;

/**
 * BaseResult
 *
 * @author Weilin Wang
 * @since 2020/4/24
 */
@Data
public class BaseResult<T> {

    public static final int SUCCESS_CODE = 0;

    private int code = 40000;
    private String msg;
    private T data;

    public static <T> BaseResult success(T t){
        BaseResult baseResult = new BaseResult();
        baseResult.setCode(SUCCESS_CODE);
        baseResult.setData(t);
        return baseResult;
    }

    public static BaseResult fail(int code, String msg){
        BaseResult baseResult = new BaseResult();
        baseResult.setCode(code);
        baseResult.setMsg(msg);
        return baseResult;
    }
}
