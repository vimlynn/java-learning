package infra.framework.common.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by wangweilin on 2018/10/29.
 */
public interface BaseMapper<R, Q> {

    R getByPrimaryKey(Long id);
    R get(@Param("entity") R record);

    int deleteByPrimaryKey(Long id);
    int del(@Param("entity") R record);
    int delByCondition(@Param("entity") R record);

    List<R> findByCondition(@Param("query") Q query);
    int findTotalCount(@Param("query") Q query);

    int save(@Param("entity") R record);
    int saveSelective(@Param("entity") R record);
    int saveBatch(@Param("entities") List<R> record);

    int updateByPrimaryKeySelective(@Param("entity") R record);
    int update(@Param("entity") R record);
}
