package infra.framework.common.service;

import infra.framework.common.domain.result.PageInfo;

import java.util.List;

/**
 * Created by wangweilin on 2018/10/29.
 */
public interface BaseService<R, Q, I> {

    I getByPrimaryKey(Long id);
    I get(R record);

    int deleteByPrimaryKey(Long id);
    int del(R record);
    int delByCondition(R record);

    List<I> listByCondition(Q query);
    PageInfo<I> findByCondition(Q query);
    int findTotalCount(Q query);

    int save(R record);
    int saveSelective(R record);
    int saveBatch(List<R> record);

    int updateByPrimaryKeySelective(R record);
    int update(R record);
}
