package infra.framework.common.converter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;

/**
 * BaseConverter
 *
 * @author Weilin Wang
 * @since 2020/4/24
 */
@Slf4j
public class BaseConverter {

    public static <R, I> I copyProperties(R r, Class<I> iClass) {
        I i;
        try {
            i = iClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            log.error("copyProperties Exception", e);
            throw new RuntimeException("copyProperties Exception");
        }
        BeanUtils.copyProperties(r, i);
        return i;
    }

}
