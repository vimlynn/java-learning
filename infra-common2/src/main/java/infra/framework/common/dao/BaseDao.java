package infra.framework.common.dao;

import java.util.List;

/**
 * Created by wangweilin on 2018/10/29.
 */
public interface BaseDao<R, Q> {

    R getByPrimaryKey(Long id);
    R get(R record);

    int deleteByPrimaryKey(Long id);
    int del(R record);
    int delByCondition(R record);

    List<R> findByCondition(Q query);
    int findTotalCount(Q query);

    int save(R record);
    int saveSelective(R record);
    int saveBatch(List<R> record);

    int updateByPrimaryKeySelective(R record);
    int update(R record);
}
