package infra.framework.common.dao.impl;

import infra.framework.common.dao.BaseDao;
import infra.framework.common.mapper.BaseMapper;
import lombok.Data;

import java.util.List;

/**
 * Created by wangweilin on 2018/10/29.
 */
@Data
public class AbstractBaseDao<R, Q, M extends BaseMapper<R, Q>> implements BaseDao<R, Q> {

    private M mapper;

    public R getByPrimaryKey(Long id) {
        return getMapper().getByPrimaryKey(id);
    }

    public R get(R record) {
        return getMapper().get(record);
    }

    public int deleteByPrimaryKey(Long id) {
        return getMapper().deleteByPrimaryKey(id);
    }

    public int del(R record) {
        return getMapper().del(record);
    }

    public int delByCondition(R record) {
        return getMapper().delByCondition(record);
    }

    public List<R> findByCondition(Q query) {
        return getMapper().findByCondition(query);
    }

    public int findTotalCount(Q query) {
        return getMapper().findTotalCount(query);
    }

    public int save(R record) {
        return getMapper().save(record);
    }

    public int saveSelective(R record) {
        return getMapper().saveSelective(record);
    }

    public int saveBatch(List<R> record) {
        return getMapper().saveBatch(record);
    }

    public int updateByPrimaryKeySelective(R record) {
        return getMapper().updateByPrimaryKeySelective(record);
    }

    public int update(R record) {
        return getMapper().update(record);
    }
}
