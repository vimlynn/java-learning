package com.vilynn.learning;

import java.util.ArrayList;
import java.util.List;

/**
 * LambdaMain
 *
 * @author weilin
 * @since 2021/3/5
 */
public class LambdaMain {

    public static void main(String[] args) {
        List<String> list1 = new ArrayList<>();
        list1.add("a");
        list1.add("b");
        list1.add("c");

        List<String> list = new ArrayList<>();
        list1.add("1");
        list1.add("2");
        list1.add("3");

        list.forEach(x -> {
            list1.forEach(y -> {

            });
        });

    }
}
