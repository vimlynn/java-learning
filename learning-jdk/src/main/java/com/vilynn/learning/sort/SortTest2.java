package com.vilynn.learning.sort;

import com.alibaba.fastjson.JSON;

/**
 * Created by wangweilin on 2019/10/25.
 */
public class SortTest2 {
    public static void main(String[] args) {
        int[] a = new int[]{-1,2,5,8,23,-1,-3-88,977};
        System.out.println(JSON.toJSONString(a));
//        bubbleSort2(a);
        insertSort(a);
//        quickSort(a);
        System.out.println(JSON.toJSONString(a));
    }

    private static void insertSort(int[] a) {
        for (int i = 1; i < a.length; i++) {
            int min = i;
            for (int j = 0; j < a.length; j++) {
                if(a[j] < a[min]){
                    min = j;
                }
            }
            a[i] = a[min];
        }
    }

    private static void bubbleSort2(int[] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = i+1; j < a.length; j++) {
                if(a[j] < a[i]){
                    int tem = a[i];
                    a[i] = a[j];
                    a[j] = tem;
                }
            }
        }
    }

    private static void quickSort(int[] a) {

    }
}
