package com.vilynn.learning.sort;

/**
 * Created by weilin.wang on 2018/3/3
 */
public class BubbleSort implements SortService {
    @Override
    public int[] sort(int[] arrs) {
        for (int i = 1; i < arrs.length; i++) {
            for (int j = 0; j < arrs.length-i; j++) {
                if(arrs[j] > arrs[j+1]){
                    int temp = arrs[j];
                    arrs[j] = arrs[j+1];
                    arrs[j+1] = temp;
                }
            }
        }
        return arrs;
    }
}