package com.vilynn.learning.sort;

/**
 * Created by weilin.wang on 2018/3/3
 */
public interface SortService {
    int[] sort(int[] arrs);
}