package com.vilynn.learning.sort;

/**
 * Created by weilin.wang on 2018/3/29
 */
public class SortBySelf {

    public static void bubbleSort(int[] arr){
        for(int i = 1;i<arr.length;i++){
            for (int j = 0; j < arr.length-i; j++) {
                if(arr[j] > arr[j+1]){
                    int temp  = arr[j+1];
                    arr[j+1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

    public static void insertSort(int[] arr){
        for(int i = 1;i<arr.length;i++){
            int inserted = arr[i];
            int j=i-1;
            for(;j>=0 && arr[j]>inserted;j--){
                arr[j+1] = arr[j];
            }
            arr[j+1] = inserted;
        }
    }

    public static void selectInsert(int[] arr){
        for(int i = arr.length-1;i>0;i--){
            int max = arr[i];
            for(int j=i-1;j>=0;j--){
                if(arr[j] > max){
                    arr[i] = arr[j];
                    arr[j] = max;
                    max = arr[j];
                }
            }
        }
    }




}


