package com.vilynn.learning.sort;

/**
 * Created by weilin.wang on 2018/3/3
 */
public class SelectSort implements SortService {
    @Override
    public int[] sort(int[] arrs) {
        for(int i=arrs.length-1; i>=0; i--){
            int max = arrs[i];
            for(int j=i-1; j>=0; j--){
                if(max < arrs[j]){
                    arrs[i] = arrs[j];
                    arrs[j] = max;
                    max = arrs[i];
                }
            }
        }
        return arrs;
    }
}