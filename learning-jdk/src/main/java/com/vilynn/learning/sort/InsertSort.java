package com.vilynn.learning.sort;

/**
 * Created by weilin.wang on 2018/3/3
 */
public class InsertSort implements SortService {
    @Override
    public int[] sort(int[] arrs) {
        for (int i = 1; i < arrs.length; i++) {
            int temp = arrs[i];
            int j = i-1;
            while (j >= 0 && temp < arrs[j]) {
                arrs[j+1] = arrs[j];
                j--;
            }
            arrs[j+1] = temp;
        }
        return arrs;
    }

    public int[] sort1(int[] arrs) {
        for (int i = 1; i < arrs.length; i++) {
            int temp = arrs[i];
            int index = i-1;
            for (int j = i-1; j >= 0; j--) {
                if(temp < arrs[j]){
                    arrs[j+1] = arrs[j];
                    index--;
                }
            }
            arrs[index+1] = temp;
        }
        return arrs;
    }

    public void sort2(int[] arrs) {
        for(int i=1;i<arrs.length;i++)
        {
            int j=i-1;
            int insert=arrs[i];
            for(;j>=0&&arrs[j]>insert;j--)
            {
                arrs[j+1]=arrs[j];
            }
            arrs[j+1]=insert;
        }
    }


}