package com.vilynn.learning.sort;

/**
 * Created by weilin.wang on 2018/3/3
 */
public class ShellSort implements SortService {
    @Override
    public int[] sort(int[] arrs) {
        for (int i = arrs.length/2; i>=1; i/=2) {
            for (int r = i; r < arrs.length; r++) {
                int temp = arrs[r];
                int j = r-i;
                while (j>=0 && temp<arrs[j]){
                    arrs[j+i] = arrs[j];
                    j -= i;
                }
                arrs[j+i] = temp;
            }

        }
        return arrs;
    }
}