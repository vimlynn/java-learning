package com.vilynn.learning.sort;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by weilin.wang on 2018/3/3
 */
//@Slf4j
public class SortTest {

    public static void main(String[] args) {
        int[] arrs = generateArrs(7);
        System.out.println(JSON.toJSONString(arrs));
//        SortBySelf.selectInsert(arrs);
        new QuickSort().sort(arrs);
        System.out.println(JSON.toJSONString(arrs));

        List<SortService> list = new ArrayList<>();
        list.add(new BubbleSort());
        list.add(new InsertSort());
        list.add(new SelectSort());
        list.add(new ShellSort());
        list.add(new QuickSort());

        for(SortService sortService : list){
            long start = System.currentTimeMillis();
            sortService.sort(arrs);
            long end = System.currentTimeMillis();
//            log.info("----------{}:时间差毫秒:{}", sortService.getClass().getName(), end-start);
//            log.info("----------结果:{}", JSON.toJSONString(Arrays.asList(arrs)));
        }
    }

    private static int[] generateArrs(int length){
        int[] arrs = new int[length];
        for (int i = 0; i < length; i++) {
            arrs[i] = i-(int)(Math.random()*1000);
        }
        return new int[]{5,2,3,7,23,2,9,8};
    }

}