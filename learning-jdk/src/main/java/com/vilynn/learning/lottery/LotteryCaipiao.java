package com.vilynn.learning.lottery;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author weilin.wang
 */
public class LotteryCaipiao {
    private static final String url = "http://gf1.yc3031.com:90/Shared/GetNewPeriod?gameid=26";
    private static String forecastNumber = randomNumber();
    private static FileWriter fw;

    static {
        try {
            fw = new FileWriter(System.getProperty("user.dir") + "/shishicai.txt", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        fw.write("\n********************************************\n");
        fw.write(String.format("=====第%s期预测号码:%s", "20180830023", forecastNumber));
        fw.flush();
        scheduler.scheduleAtFixedRate(new Make(), 0, 10, TimeUnit.MINUTES);
    }

    private static class Make implements Runnable {
        private int i = 0;

        @Override
        public void run() {
            JSONObject result = winNumber();
            String winNumber = result.getString("fpreviousresult");
            long issueId = result.getLong("fpreviousperiod");
            boolean isWin = win(forecastNumber, winNumber);
            String s = String.format("开奖号码:%s  %s \n", winNumber, isWin);
            try {
                fw.write(s);
                fw.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(isWin){
            }else{
                i++;
            }
            if(isWin || i>1){
                forecastNumber = randomNumber();
                i = 0;
            }
            try {
                fw.write(String.format("=====第%s期预测号码:%s", issueId + 1, forecastNumber));
                fw.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static boolean win(String forecastNumber, String winNumber) {
        if(forecastNumber.contains(String.valueOf(winNumber.split(",")[2]))
                && forecastNumber.contains(String.valueOf(winNumber.split(",")[3]))
                && forecastNumber.contains(String.valueOf(winNumber.split(",")[4]))
                && isGroupSix(winNumber)){
            return true;
        }
        return false;
    }

    private static boolean isGroupSix(String winNumber) {
        if(winNumber.charAt(2) == winNumber.charAt(3)
                || winNumber.charAt(2) == winNumber.charAt(4)
                || winNumber.charAt(3) == winNumber.charAt(4)){
            return false;
        }
        return true;
    }

    private static String randomNumber() {
        Random random = new Random();
        String s = "0123456789";
        do {
            int n1 = random.nextInt(9);
            s = s.replace(String.valueOf(n1), "");
        } while (s.length() > 8);
        return s;
    }

    private static JSONObject winNumber(){
        RestTemplate restTemplate = new RestTemplate();
//        JSONObject res;
        try {
            String res = restTemplate.getForObject(url, String.class);
            return JSON.parseObject(res);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                Thread.sleep(3000L);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            winNumber();
        }
        return new JSONObject();
//        HttpClient httpClient = new DefaultHttpClient();
//        byte[] bytes = new byte[1024];
//        try {
//            HttpGet httpGet = new HttpGet(new URI(url));
//            HttpResponse httpResponse = httpClient.execute(httpGet);
//            InputStream content = httpResponse.getEntity().getContent();
//
//            content.read(bytes);
//
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        } catch (ClientProtocolException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        String s = new String(bytes);
//        System.out.println(s);
//        Object result = JSON.parseObject(s);
//        return (JSONObject) result;
    }

    @Data
    public static class Result {
        private boolean IsEnabled;
        private boolean fisstopseles;
        private int gameID;
        private int fid;
        private int fnumberofperiod;
        private int fnextperiod;
        private int fstatus;
        private int fsettlefid;
        private int fsettlenumber;
        private String fsettletime;
        private String fnextstarttime;
        private String fclosetime;
        private String fstarttime;
        private String flottostarttime;
        private String ServerTime;
        private int fpreviousperiod;
        private String fpreviousresult;
    }
}
