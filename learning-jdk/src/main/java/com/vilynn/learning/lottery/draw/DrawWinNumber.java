package com.vilynn.learning.lottery.draw;

/**
 * @author weilin.wang
 */
public interface DrawWinNumber {
    String winNumber();
}
