package com.vilynn.learning.lottery;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 * @author weilin.wang
 */
public class LotteryMoney2 {
    private static final String url = "https://chart.cp.360.cn/zst/qkj/?lotId=255401&issue=000";
    private static final String url2= "http://yc3033.com:90/Shared/GetNewPeriod?gameid=123&_=1535075405631";
    private static String forecastNumber = randomNumber();
    private static FileWriter fw;
    private static String type = "fenfencai";

    static {
        try {
            fw = new FileWriter("/Users/wangweilin/Documents/fenfencai_qian2.txt", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test(){
        System.out.println(win("11", "77929"));
    }

    public static void main(String[] args) throws Exception {
        if(StringUtils.isNotEmpty(System.getProperty("type"))){
            type = System.getProperty("type");
        }
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(new Make(), 0, 1, TimeUnit.MINUTES);
        fw.write("======================"+type+"======================\n");
    }

    private static class Make implements Runnable {
        private int i = 0;

        @Override
        public void run() {
            String[] res;
            if("shishicai".equals(type)){
                res = LotteryMoney.winNumber().split("-");
            }else{
                res = LotteryMoney.winNumber2().split("-");
            }
            String winNumber = res[1];
            boolean isWin = win(forecastNumber, winNumber);
            String s = String.format("=====第%s期前二预测号码:[%s],开奖号码:%s,%s\n", res[0], forecastNumber+print(forecastNumber), winNumber, isWin);
            try {
                fw.write(s);
                fw.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(isWin){
            }else{
                i++;
            }
            if(isWin || i>1){
                forecastNumber = randomNumber();
                i = 0;
            }
        }
    }

    public static boolean win(String forecastNumber, String winNumber) {
        if(Integer.valueOf(winNumber.charAt(0)) %2 == Integer.valueOf(forecastNumber.split("")[0])
            && Integer.valueOf(winNumber.charAt(1)) %2 == Integer.valueOf(forecastNumber.split("")[1])){
            return true;
        }
        return false;
    }

    public static String randomNumber() {
        Random random = new Random();
        String[] arr = new String[2];
        for (int i = 0; i < 2; i++) {
            if(random.nextInt() %2 == 0){
                arr[i] = "0";
            }else{
                arr[i] = "1";
            }
        }
        return Arrays.stream(arr).collect(Collectors.joining(""));
    }

    public static String print(String s){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            if("0".equals(s.split("")[i])){
                sb.append("双");
            }else{
                sb.append("单");
            }
            sb.append(",");
        }
        return sb.toString();
    }
}
