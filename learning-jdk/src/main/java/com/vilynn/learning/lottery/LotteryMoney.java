package com.vilynn.learning.lottery;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author weilin.wang
 */
public class LotteryMoney {
    private static final String url = "https://chart.cp.360.cn/zst/qkj/?lotId=255401&issue=000";
    private static final String url2= "http://yc3033.com:90/Shared/GetNewPeriod?gameid=123&_=1535075405631";
    private static String forecastNumber = randomNumber();
    private static FileWriter fw;
    private static String type = "fenfencai";

    static {
        try {
            fw = new FileWriter("/Users/wangweilin/Documents/shishicai.txt", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public LotteryMoney() throws IOException {
    }

    @Test
    public void test(){
        for (int i = 0; i < 100; i++) {

            System.out.println(randomNumber());
        }
    }

    public static void main(String[] args) throws Exception {
        if(StringUtils.isNotEmpty(System.getProperty("type"))){
            type = System.getProperty("type");
        }
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(new Make(), 0, 1, TimeUnit.MINUTES);
        fw.write("======================"+type+"======================\n");
    }

    private static class Make implements Runnable {
        private int i = 0;

        @Override
        public void run() {
            String[] res;
            if("shishicai".equals(type)){
                res = winNumber().split("-");
            }else{
                res = winNumber2().split("-");
            }
            String winNumber = res[1];
            boolean isWin = win(forecastNumber, winNumber);
            String s = String.format("=====第%s期预测号码:%s,开奖号码:%s,%s\n", res[0], forecastNumber, winNumber, isWin);
            try {
                fw.write(s);
                fw.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(isWin){
            }else{
                i++;
            }
            if(isWin || i>1){
                forecastNumber = randomNumber();
                i = 0;
            }
        }
    }

    public static boolean win(String forecastNumber, String winNumber) {
        if(forecastNumber.contains(String.valueOf(winNumber.charAt(2)))
                && forecastNumber.contains(String.valueOf(winNumber.charAt(3)))
                && forecastNumber.contains(String.valueOf(winNumber.charAt(4)))
                && isGroupSix(winNumber)){
            return true;
        }
        return false;
    }

    public static boolean isGroupSix(String winNumber) {
        if(winNumber.charAt(2) == winNumber.charAt(3)
                || winNumber.charAt(2) == winNumber.charAt(4)
                || winNumber.charAt(3) == winNumber.charAt(4)){
            return false;
        }
        return true;
    }

    public static String randomNumber() {
        Random random = new Random();
        String s = "0123456789";
        do {
            int n1 = random.nextInt(10);
            s = s.replace(String.valueOf(n1), "");
        } while (s.length() > 8);
        return s;
    }

    public static String winNumber(){
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<JSONObject> res = restTemplate.getForEntity(url, JSONObject.class);
        return res.getBody().getString("preIssue")
                + "-" + res.getBody().getObject("0", LinkedHashMap.class).get("WinNumber").toString();
    }

    public static String winNumber2(){
        HttpClient httpClient = new DefaultHttpClient();
        HttpUriRequest httpUriRequest = new HttpGet(url2);
        try {
            HttpResponse httpResponse = httpClient.execute(httpUriRequest);
            InputStream is = httpResponse.getEntity().getContent();
            byte[] bs = new byte[2048];
            is.read(bs);
            JSONObject j = JSON.parseObject(new String(bs));
            return j.getString("fpreviousperiod")
                    + "-" + j.getString("fpreviousresult").replace(",", "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "999-12345";
    }
}
