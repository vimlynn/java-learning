package com.vilynn.learning.lottery;

/**
 * @author weilin.wang
 * @version v1.0
 * @date 2018/8/24
 * @see
 * @since 1.0
 */
public enum LotteryEnum {
    TOPSPEED_MINUTE,
    CHONGQING_HOUR
}
