package com.vilynn.learning.lottery.new1206;

import com.vilynn.learning.lottery.DrawNumber;
import org.junit.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by wangweilin on 2018/12/6.
 */
public class Lottery1206 {
    private static FileWriter fw;

    static {
        try {
            fw = new FileWriter("/Users/wangweilin/Documents/document/caipiao1206.txt", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testList() throws Exception {
        List<DrawNumber> list = DrawNumber.list();
        list.forEach(drawNumber -> {
            String randomWinNumber = random();
            System.out.println(randomWinNumber);

            compare(randomWinNumber, drawNumber);
        });
    }

    @Test
    public void test() throws InterruptedException {

        String randomWinNumber1 = random();
        System.out.println(randomWinNumber1);

        DrawNumber drawNumber1 = DrawNumber.get();
//        System.out.println(drawNumber1);

        compare(randomWinNumber1, drawNumber1);

        while (true) {

            String randomWinNumber = random();
            System.out.println(randomWinNumber);
            Thread.sleep(10 * 60 * 1000);

            DrawNumber drawNumber = DrawNumber.get();
//            System.out.println(drawNumber);

            compare(randomWinNumber, drawNumber);
        }
    }

    private static int yin = 0;
    private static int yin6 = 0;
    private static int yin3 = 0;

    private static void compare(String randomWinNumber, DrawNumber drawNumber) {
        boolean is = false;
        String number = drawNumber.getDrawNumber().substring(3);
        if(!number.contains(randomWinNumber.split(",")[0])
                && !number.contains(randomWinNumber.split(",")[1])){
            is = true;
        }
        String s = String.format("=====第%s期后三预测号码:杀[%s],开奖号码:%s,%s,%s",
                drawNumber.getIssue(), randomWinNumber, drawNumber.getDrawNumber(),
                drawNumber.isGroupSix() ? "组六" : "组三",
                is ? "中":"无");
        System.out.println(s);
        try {
            fw.write(s + "\n");
            fw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(is){
            yin++;
        }
        if(is && drawNumber.isGroupSix()){
            yin6++;
        }
        if(is && !drawNumber.isGroupSix()){
            yin3++;
        }
        System.out.println(String.format("🀄️%d期，组三赢%d期，组六赢%d期", yin, yin3, yin6));
    }

    private static String random() {
        int s = (int)(Math.random() * 10);
        int g;
        do {
            g = (int)(Math.random() * 10);
        } while (s == g);
        return s + "," + g;
    }

}
