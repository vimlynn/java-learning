package com.vilynn.learning.lottery;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wangweilin on 2018/12/6.
 */
@Data
@NoArgsConstructor
public class DrawNumber {
    private static final String url = "http://gf2.xun1616.com:90/Shared/GetNewPeriod?gameid=26&_=1544064191882";
    private static final String url_list = "http://gf2.xun1616.com:90/Result/GetLotteryResultList?gameID=26&pageSize=100&pageIndex=1&_=1544074670695";
    private String issue;
    private String drawNumber;
    private boolean groupSix;

    public boolean isGroupSix() {
        String[] drawNumber = this.drawNumber.substring(3).split(",");
        if(!drawNumber[0] .equals( drawNumber[1])
                && !drawNumber[0] .equals( drawNumber[2])
                && !drawNumber[2] .equals( drawNumber[1])){
            return true;
        }
        return false;
    }

    public DrawNumber(String issue, String drawNumber) {
        this.issue = issue;
        this.drawNumber = drawNumber;
    }

    public static DrawNumber get(){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new FastJsonHttpMessageConverter());
        try {
            Res forObject = restTemplate.getForObject(new URI(url), Res.class);
            return new DrawNumber(forObject.getFpreviousperiod(), forObject.getFpreviousresult());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<DrawNumber> list(){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new FastJsonHttpMessageConverter());
        try {
            ResList forObject = restTemplate.getForObject(new URI(url_list), ResList.class);
            return convert(forObject);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static List<DrawNumber> convert(ResList resList) {
        List<DrawNumber> list = new ArrayList<>();
        for (ResList.Res res : resList.getList()) {
            list.add(new DrawNumber(res.getPeriod(), res.getResult()));
        }
        return list;
    }

    @Data
    private static class Res {
        private String IsEnabled;
        private String fisstopseles;
        private String gameID;
        private String fid;
        private String fnumberofperiod;
        private String fnextperiod;
        private String fstatus;
        private String fsettlefid;
        private String fsettlenumber;
        private String fsettletime;
        private String fnextstarttime;
        private String fclosetime;
        private String fstarttime;
        private String flottostarttime;
        private String ServerTime;
        private String fpreviousperiod;
        private String fpreviousresult;
        private String fpreviousanimal;
    }

    @Data
    private static class ResList {
        private int total;
        private List<Res> list;
        @Data
        private static class Res {
            private String id;
            private String period;
            private String date;
            private String result;
            private String year;
        }
    }

}
