package com.vilynn.learning.juc.forkjoin.service;

import com.google.common.collect.Lists;
import com.vilynn.learning.pojo.FunObject;

import java.util.List;

public class RemoteService {

    /**
     * 模拟耗时1秒
     * @param id
     * @return
     */
    public List<FunObject> getList(String id) {
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Lists.newArrayList(new FunObject());
    }

    /**
     * 模拟耗时1秒
     * @param id
     * @return
     */
    public FunObject get(String id) {

        try {
            Thread.sleep(800L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        FunObject funObject = new FunObject();
        funObject.setAge(0);
        funObject.setName("");
        funObject.setSx(false);
        funObject.setFunObject(new FunObject());

        return funObject;
    }

}
