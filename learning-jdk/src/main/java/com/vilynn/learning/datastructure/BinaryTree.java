package com.vilynn.learning.datastructure;

/**
 * 数据结构二叉树
 */
public class BinaryTree {
    char node;
    BinaryTree leftNode;
    BinaryTree rightNode;

    public BinaryTree(char node) {
        this.node = node;
    }

    public static BinaryTree init() {
        BinaryTree binaryTree = new BinaryTree('A');
        binaryTree.leftNode = new BinaryTree('B');
        binaryTree.rightNode = new BinaryTree('C');

        binaryTree.leftNode.leftNode = new BinaryTree('D');
        binaryTree.leftNode.leftNode.leftNode = new BinaryTree('F');
        binaryTree.leftNode.leftNode.rightNode = new BinaryTree('G');

        binaryTree.rightNode.rightNode = new BinaryTree('E');
        binaryTree.rightNode.rightNode.rightNode = new BinaryTree('H');
        return binaryTree;
    }

    public static void main(String[] args) {
        BinaryTree binaryTree = init();
        print(binaryTree);
    }

    private static void print(BinaryTree binaryTree) {
        if (binaryTree == null) {
            return;
        }
        char node = binaryTree.node;
        if (node > 0) {
            System.out.println(node);
        }
        print(binaryTree.leftNode);
        print(binaryTree.rightNode);
    }

}
