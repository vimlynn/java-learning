package com.vilynn.learning.framework;

import com.github.rholder.retry.*;
import com.vilynn.learning.pojo.FunObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author weilin
 * @since 2020/12/16
 */
public class RetryMain {

    public static void main(String[] args) throws ExecutionException, RetryException, InterruptedException {
        RetryListener listener = new RetryListener() {
            @Override
            public <V> void onRetry(Attempt<V> attempt) {

            }
        };
        Retryer<Object> retryer = RetryerBuilder.newBuilder()
                .retryIfException()
                .retryIfResult(res -> new ArrayList<>().isEmpty())
                .withWaitStrategy(WaitStrategies.incrementingWait(3, TimeUnit.SECONDS, 1, TimeUnit.SECONDS))
                .withStopStrategy(StopStrategies.stopAfterAttempt(5))
                .withRetryListener(listener)
                .build();

        retryer.call(() -> {
            System.out.println("wmwwmmw" + new Date().toString());
            return new FunObject();
        });

        Thread.sleep(100000L);
    }
}
