package com.vilynn.learning.collection;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

/**
 * MapTest
 *
 * @author weilin
 * @since 2020/12/30
 */
public class MapTest {

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("a", 1);
        map.put("b", 2);
        map.put("c", 3);

        System.out.println(JSON.toJSONString(map));
        map.forEach((k,v) -> {
            map.put(k, 10);
        });
        System.out.println(JSON.toJSONString(map));
    }
}
