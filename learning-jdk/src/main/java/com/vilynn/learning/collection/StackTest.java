package com.vilynn.learning.collection;

import lombok.Data;
import org.junit.Test;

import java.util.Stack;

/**
 * Created by wangweilin on 2019/5/27.
 */
@Data
public class StackTest {
    private Stack stack = new Stack();
    public Object pop(){
        return stack.pop();
    }
    public Object push(Object obj){
        return stack.push(obj);
    }

    @Test
    public void t(){
        StackTest test = new StackTest();
        test.push(1);
        test.push(2);
        test.push(3);

        System.out.println(test.pop());
    }
}
