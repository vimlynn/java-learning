package com.vilynn.learning.collection;

import com.alibaba.fastjson.JSON;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by weilin.wang on 2017/5/18
 */
public class Test {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(232);
        list.add(6);
        list.add(0);
        list.add(8);
        list.add(null);
        list.add(22);
        Collections.sort(list, new Comparator<Integer>() {
            @Override
            public int compare(Integer card1, Integer card2) {
                return compareInteger(card1, card2);
//                return compare(BigDecimal.valueOf(card1), BigDecimal.valueOf(card2));
            }

            private int compareInteger(Integer amount1, Integer amount2) {
                amount1 = amount1 == null ? 0 : amount1;
                amount2 = amount2 == null ? 0 : amount2;
                return amount1 > amount2 ? -1 : 0;
            }

            private int compare(BigDecimal amount1, BigDecimal amount2) {
                amount1 = amount1 == null ? BigDecimal.ZERO : amount1;
                amount2 = amount2 == null ? BigDecimal.ZERO : amount2;
                return amount2.compareTo(amount1);
            }
        });

        System.out.println(JSON.toJSONString(list));
    }
}
