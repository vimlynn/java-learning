package com.vilynn.learning.collection;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.util.*;

/**
 * Created by wangweilin on 2019/5/27.
 */
public class ListTest {

    @Test
    public void test(){
        List list = new ArrayList();
        list.add(1);
        list.add(2);

        int[] arr = new int[Integer.MIN_VALUE];
        list.add(Arrays.asList(arr));
    }

    public LinkedList convert(LinkedList l){
        LinkedList res = new LinkedList();
        Iterator it = l.iterator();
        while (it.hasNext()){
            Object next = it.next();
            LinkedList temp = new LinkedList();
            temp.add(next);
            temp.addAll(res);

            res = temp;
        }
        return res;
    }

    @Test
    public void t(){
        LinkedList a = new LinkedList();
        a.add(1);
        a.add(2);
        a.add(3);
        a.add(4);
        a.add(5);
        System.out.println(JSON.toJSONString(a));
        LinkedList b = convert(a);
        System.out.println(JSON.toJSONString(b));
    }
}
