package com.vilynn.learning;

import java.math.BigDecimal;

/**
 * @author weilin.wang
 */
public class MathTest {

    public static void main(String[] args) {
        float f = 1.7f;
        BigDecimal b = BigDecimal.valueOf(f);
        System.out.println(String.valueOf(b));
    }

}
