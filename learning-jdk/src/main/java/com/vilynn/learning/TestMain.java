package com.vilynn.learning;

/**
 * TestMain
 *
 * @author weilin
 * @since 2021/3/8
 */
public class TestMain {

    private Thread thread;

    public void method() {
        String s = "1";
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(s);
            }
        });

        this.thread = thread;
        thread.start();

    }


}
