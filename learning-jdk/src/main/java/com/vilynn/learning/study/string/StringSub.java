package com.vilynn.learning.study.string;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by weilin.wang on 2017/7/27
 */
public class StringSub {

    public static String stringReplace3(String str) {
        if (StringUtils.isEmpty(str)) {
            return StringUtils.EMPTY;
        }

        int dstPos = 0;
        char result[] = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != '\b') {
                result[dstPos++] = str.charAt(i);
            } else {
                if (dstPos > 0) {
                    dstPos--;
                }
            }
        }

        return String.valueOf(result, 0, dstPos);
    }


    public static void main(String[] args) {
        System.out.println(StringSub.stringReplace3("abc\\b\\bd\\b\\bghi"));
    }
}
