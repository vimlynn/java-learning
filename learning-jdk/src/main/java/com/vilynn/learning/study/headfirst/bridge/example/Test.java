package com.vilynn.learning.study.headfirst.bridge.example;

/**
 * Created by weilin.wang on 2017/5/24
 */
public class Test {
    public static void main(String[] args) {
        MessageSMS messageSMS = new MessageSMS();
        messageSMS.send("模式", "weilin");

        CommonMessage commonMessage = new CommonMessage(new MessageSMS());
        commonMessage.sendMessage("模式", "weilin");

        CommonMessage commonMessage1 = new CommonMessage(new MessageMobile());
        commonMessage1.sendMessage("模式", "weilin");

        AbstractMessage commonMessage2 = new SpecialUrgencyMessage(new MessageEmail());
        commonMessage2.sendMessage("模式", "weilin");
    }
}
