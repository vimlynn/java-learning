package com.vilynn.learning.study.headfirst.bridge;

/**
 * Created by weilin.wang on 2017/5/24
 */
public interface Implementor {

    /**
     * 示例方法，实现抽象部分需要的某些具体功能
     */
    public void operationImpl();
}
