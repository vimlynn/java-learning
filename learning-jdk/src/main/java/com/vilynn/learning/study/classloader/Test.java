package com.vilynn.learning.study.classloader;

/**
 * Created by weilin.wang on 2017/5/18
 */
public class Test
{
    static
    {
        i=0;
//        System.out.println(i);//这句编译器会报错：Cannot reference a field before it is defined（非法向前应用）
    }
    static int i=1;
}
