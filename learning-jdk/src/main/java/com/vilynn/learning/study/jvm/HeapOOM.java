package com.vilynn.learning.study.jvm;

import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by weilin.wang on 2017/5/16
 * VM Args:-Xms20M -Xmx20M -Xmn10M -XX:SurvivorRatio=8
 */
@Log4j2
public class HeapOOM {
    static class OOMObject {
    }

    public static void main(String[] args) {

        try {
            log.error("----start---" + new Date().toString());
            List<OOMObject> list = new ArrayList<>();
            while (true) {
                list.add(new OOMObject());
            }
        } catch (Error e) {
            log.error("----end---" + new Date().toString(), e);
//            e.printStackTrace();
        }
    }
}
