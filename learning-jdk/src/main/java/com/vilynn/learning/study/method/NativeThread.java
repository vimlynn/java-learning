package com.vilynn.learning.study.method;

/**
 * Created by weilin.wang on 2017/5/25
 */
public class NativeThread extends Thread {
    @Override
    public boolean isInterrupted() {
        return isInterrupted(false) == true;
    }

    private native boolean isInterrupted(boolean ClearInterrupted);

    static{

        System.loadLibrary("vtkCommonJava");
        System.loadLibrary("vtkFilteringJava");
        System.loadLibrary("vtkIOJava");
        System.loadLibrary("vtkImagingJava");
        System.loadLibrary("vtkGraphicsJava");
        System.loadLibrary("vtkRenderingJava");

    }
}
