package com.vilynn.learning.study.classloader;

/**
 * Created by weilin.wang on 2017/5/18
 */
interface Super2{
    public static int m = 11;

}

class Father2 implements Super2{
    public static int m = 33;
    static{
        System.out.println("执行了父类静态语句块");
    }
}

class Child2 extends Father2 implements Super2{
    static{
        System.out.println("执行了子类静态语句块");
    }
}

public class StaticTest2{
    public static void main(String[] args){
//        System.out.println(Child2.m); //报错
    }
}
