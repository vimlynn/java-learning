package com.vilynn.learning.study.headfirst.bridge;

/**
 * Created by weilin.wang on 2017/5/24
 */
public class ConcreteImplementorB implements Implementor {

    @Override
    public void operationImpl() {
        // 真正的实现

    }

}
