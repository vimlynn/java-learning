package com.vilynn.learning.study.math;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by weilin.wang on 2017/12/24
 */
public class PerfactNumber {

    public static List<Integer> get(int top){
        List<Integer> res = new ArrayList<>();
        for(int i=1;i<=top;i++){
            List<Integer> yinzi = getYinzi(i);
            int temp = 0;
            for(Integer tp : yinzi){
                temp += tp;
            }
            if(temp == i){
                res.add(i);
            }
        }
        return res;
    }

    private static List<Integer> getYinzi(int number) {
        List<Integer> res = new ArrayList<>();
        for(int i=1;i<number;i++){
            if(number%i==0){
                res.add(i);
            }
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.printf(JSON.toJSONString(get(999)));
    }

}