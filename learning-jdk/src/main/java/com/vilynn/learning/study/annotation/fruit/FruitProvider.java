package com.vilynn.learning.study.annotation.fruit;

/**
 * Created by weilin.wang on 2017/5/18
 */

/**
 * 水果供应者注解
 * @author peida
 *
 */
//@Target(ElementType.FIELD)
//@Retention(RetentionPolicy.RUNTIME)
//@Documented
public @interface FruitProvider {
    /**
     * 供应商编号
     * @return
     */
    public int id() default -1;

    /**
     * 供应商名称
     * @return
     */
    public String name() default "";

    /**
     * 供应商地址
     * @return
     */
    public String address() default "";
}
