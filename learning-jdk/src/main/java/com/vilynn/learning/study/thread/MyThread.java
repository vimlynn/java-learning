package com.vilynn.learning.study.thread;

/**
 * Created by weilin.wang on 2017/5/10
 */
public class MyThread extends Thread {
    public MyThread(String name, int priority){
        super(name);
        this.setPriority(priority);
    }

    @Override
    public void run() {

        for(int i=0;i<5;i++){
            yield();
            System.out.print(this.getName() + i);
        }
    }
}
