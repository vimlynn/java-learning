package com.vilynn.learning.study.thread;

/**
 * Created by weilin.wang on 2017/6/8
 */

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class MainThread {

    public static int count = 10;
    volatile static int newint = 10;
    static CountDownLatch countDownLatch = new CountDownLatch(10);
    static CyclicBarrier barrier = new CyclicBarrier(11);


    public static void main(String[] args) throws Exception {

        long start = System.currentTimeMillis();

        countDownLatch();
        //join();
        //activeCount();
        //cyclicbarrier();
        //sleep();

        long end = System.currentTimeMillis();
        System.out.println(end - start);

    }

    private static void sleep() {

        for(int i =0;i<10;i++){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName());
                }
            }).start();
        }
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void cyclicbarrier() throws InterruptedException, BrokenBarrierException {
        for(int i =0;i<10;i++){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName());
                    try {
                        MainThread.barrier.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
        MainThread.barrier.await();
    }

    private static void activeCount() {
        for(int i =0;i<10;i++){
            new Thread(new Runnable() {

                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName());
                }
            }).start();
        }
        while(Thread.activeCount()!=1){

        }
    }

    private static void join() throws InterruptedException {
        for(int i =0;i<10;i++){

            Thread thread = new Thread(new Runnable() {

                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName());
                }
            });
            thread.start();
            thread.join();
        }
    }

    private static void countDownLatch() throws InterruptedException {
        for(int i = 0;i<10;i++)
            new Thread(new Runnable() {

                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName());
                    MainThread.countDownLatch.countDown();
                }
            }).start();

        MainThread.countDownLatch.await();
    }


}
