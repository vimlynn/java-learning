package com.vilynn.learning.study.nio;

import sun.misc.BASE64Encoder;

import javax.imageio.stream.FileImageInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by weilin.wang on 2017/5/19
 */
public class Pic {
    public static void main(String[] args) throws IOException {
        System.out.println(1 | 2324);
        System.out.println(1 & 2324);
        String path = "E:\\projects_study\\videosystem\\src\\com\\lynn\\tool\\test\\nio\\io1.png";
        FileInputStream fis = new FileInputStream(path);
        byte[] data = null;
        FileImageInputStream input = null;
        try {
            input = new FileImageInputStream(new File(path));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int numBytesRead = 0;
            while ((numBytesRead = input.read(buf)) != -1) {
                output.write(buf, 0, numBytesRead);
            }
            data = output.toByteArray();
            output.close();
            input.close();
        }
        catch (FileNotFoundException ex1) {
            ex1.printStackTrace();
        }
        catch (IOException ex1) {
            ex1.printStackTrace();
        }
        System.out.println(data.length / 1024 );
        BASE64Encoder encoder = new BASE64Encoder();
        String str = encoder.encode(data);
        System.out.println(str.length());
//        System.out.println(JSON.toJSONString(data));
//        String json = JSON.toJSONString(data);
//        byte[] d  = (byte[])JSON.parse(json);
//        for(byte b : d){
//            System.out.print("," + b);
//        }
    }

}
