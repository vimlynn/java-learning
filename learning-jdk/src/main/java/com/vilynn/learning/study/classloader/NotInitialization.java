package com.vilynn.learning.study.classloader;

/**
 * Created by weilin.wang on 2017/5/18
 */
public class NotInitialization
{
    public static void main(String[] args)
    {
        System.out.println("-------------start---------------");

        SuperClass[] sca = new SuperClass[10];
        System.out.println("------------111----------------");
        System.out.println(SubClass.value);
        System.out.println("-------------222---------------");
        System.out.println(ConstClass.HELLOWORLD);
        System.out.println("-------------333---------------");
        System.out.println(ConstClass.HELLOWORLD2);

        System.out.println("-------------end---------------");
    }
}
