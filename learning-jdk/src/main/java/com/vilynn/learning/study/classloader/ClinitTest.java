package com.vilynn.learning.study.classloader;

/**
 * Created by weilin.wang on 2017/5/18
 */
class Father3{
    public static int a = 1;
    static{
        a = 2;
    }
}

class Child3 extends Father3{
    public static int b = a;
}

public class ClinitTest{
    public static void main(String[] args){
        System.out.println(Child3.b);
    }
}
