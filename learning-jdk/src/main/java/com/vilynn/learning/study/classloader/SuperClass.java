package com.vilynn.learning.study.classloader;

/**
 * Created by weilin.wang on 2017/5/18
 */
public class SuperClass extends SSclass
{
    static
    {
        System.out.println("SuperClass init!");
    }

    public static int value = 123;

    public SuperClass()
    {
        System.out.println("init SuperClass");
    }
}
