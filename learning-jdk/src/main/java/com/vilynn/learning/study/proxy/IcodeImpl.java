package com.vilynn.learning.study.proxy;

import lombok.Data;

/**
 * Created by weilin.wang on 2017/8/11
 */
@Data
public class IcodeImpl implements Icode {

    private String name;

    public IcodeImpl(String name) {
        this.name = name;
    }

    @Override
    public void implDemand() {
        System.out.println(name + "实现了需求！");
    }
}
