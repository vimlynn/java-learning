package com.vilynn.learning.study.jvm;

import java.util.List;
import java.util.ArrayList;

/**
 * VM Args:-XX:PermSize=10M -XX:MaxPermSize=10M
 */
public class RuntimeConstantPoolOOM {
    public static void main(String[] args) {
        // 使用List保持常量池引用，避免Full GC回收常量池内的对象
        List<String> list = new ArrayList<String>();
        int i = 0;
        while (true) {
            list.add(String.valueOf((i++)).intern());
        }
    }

}
