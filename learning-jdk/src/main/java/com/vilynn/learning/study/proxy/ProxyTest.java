package com.vilynn.learning.study.proxy;

/**
 * Created by weilin.wang on 2017/8/11
 */
public class ProxyTest {

    public static void main(String[] args) {
        Icode icode = new IcodeImplProxy(new IcodeImpl("wwl"));
        icode.implDemand();
    }

}
