package com.vilynn.learning.study.nio;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by weilin.wang on 2017/5/19
 */
public class NioStart {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("E:/projects_study/videosystem/src/com/lynn/tool/test/nio/niotest.txt");
        FileChannel fileChannel = fis.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(6);

        fileChannel.read(byteBuffer);

        byteBuffer.flip();
        FileOutputStream fos = new FileOutputStream("E:/projects_study/videosystem/src/com/lynn/tool/test/nio/niotest_copy.txt");
        FileChannel fileChannel1 = fos.getChannel();
//        ByteBuffer byteBuffer1 = ByteBuffer.allocate(1024);
        fileChannel1.write(byteBuffer);

        byteBuffer.clear();
//        System.exit(0);


    }
}
