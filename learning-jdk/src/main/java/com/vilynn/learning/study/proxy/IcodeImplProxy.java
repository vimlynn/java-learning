package com.vilynn.learning.study.proxy;

import lombok.Data;

/**
 * Created by weilin.wang on 2017/8/11
 */
@Data
public class IcodeImplProxy implements Icode {
    private Icode icode;

    public IcodeImplProxy(Icode icode) {
        this.icode = icode;
    }

    @Override
    public void implDemand() {
        if(false){
            return;
        }
        icode.implDemand();
    }
}
