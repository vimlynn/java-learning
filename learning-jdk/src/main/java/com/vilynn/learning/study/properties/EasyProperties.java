package com.vilynn.learning.study.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by weilin.wang on 2017/5/11
 */
public class EasyProperties {
    public static void main(String[] args) throws Exception {
        Properties properties = new Properties();
        File file = new File("D:\\Workspaces\\videosystem\\src\\com\\lynn\\tool\\test\\properties\\properties.properties");
//        File file = new File("jdbc.properties");
        InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);

        System.out.println(properties);
    }
}
