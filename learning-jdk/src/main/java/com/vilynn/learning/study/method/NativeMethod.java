package com.vilynn.learning.study.method;

/**
 * Created by weilin.wang on 2017/5/25
 */
public class NativeMethod {

    public String getRes(){
        isInterrupted(false);
        return "";
    }

    private native boolean isInterrupted(boolean ClearInterrupted);

    public static void main(String[] args) {
//        System.out.println(new NativeMethod().getRes());

//        System.out.println(new Thread().isInterrupted());

//        System.loadLibrary("rt.jar");
        System.out.println(new NativeThread().isInterrupted());

    }
}
