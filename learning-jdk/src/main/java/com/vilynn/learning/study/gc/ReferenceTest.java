package com.vilynn.learning.study.gc;

import java.lang.ref.WeakReference;

/**
 * VM Args:-XX:+PrintHeapAtGC
 */
public class ReferenceTest {

    public static void main(String[] args) {
        byte[] bytes = new byte[1024 * 1024 * 5];
        /**
         * 取消softBytes所在行注释，在内存充足的情况下，GC不会回收所关联的byte数组。
         */
//         SoftReference<byte[]> softBytes=new SoftReference<byte[]>(bytes);
        /**
         * 取消weakBytes所在行注释，在内存充足的情况下，GC也会成功回收所关联的byte数组。
         */
         WeakReference<byte[]> weakBytes=new WeakReference<byte[]>(bytes);
        bytes = null;
        System.gc();
    }
}
