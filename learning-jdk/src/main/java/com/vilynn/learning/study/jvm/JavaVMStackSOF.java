package com.vilynn.learning.study.jvm;

import com.alibaba.fastjson.JSON;

/**
 * Created by weilin.wang on 2017/5/16
 * VM Args:-Xss128k
 */
public class JavaVMStackSOF {
    private int stackLength = 1;

    public void stackLeak() {
//        int x = 8;
        stackLength++;
        stackLeak();
    }

    public static void main(String[] args) throws Throwable {
        System.out.println(JSON.toJSON(System.getProperty("-Xss")));
        JavaVMStackSOF sof = new JavaVMStackSOF();
        try {
            sof.stackLeak();
        } catch (Throwable e) {
            System.out.println("stack length:" + sof.stackLength);
            throw e;
        }
    }
}
