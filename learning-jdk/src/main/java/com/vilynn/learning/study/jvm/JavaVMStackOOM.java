package com.vilynn.learning.study.jvm;

/**
 * Created by weilin.wang on 2017/5/16
 * VM Args:-Xss4M(这时候不妨设置大些)
 */
public class JavaVMStackOOM {
    private void dontStop() {
        while (true) {
        }
    }

    public void stackLeakByThread() {
        while (true) {
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    dontStop();
                }
            });
            thread.start();
            System.out.println(Thread.activeCount());
        }
    }

    public static void main(String[] args) throws Throwable {
        JavaVMStackOOM oom = new JavaVMStackOOM();
        oom.stackLeakByThread();
    }
}
