package com.vilynn.learning.study.classloader;

/**
 * Created by weilin.wang on 2017/5/18
 */
public class ConstClass
{
    static
    {
        System.out.println("ConstClass init!");
    }
    public static  final String HELLOWORLD = "hello world";
    public static  String HELLOWORLD2 = "no final hello world";
}
