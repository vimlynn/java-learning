package com.vilynn.learning.study.annotation.fruit;

/**
 * Created by weilin.wang on 2017/5/18
 */
/***********注解声明***************/

/**
 * 水果名称注解
 * @author peida
 *
 */
//@Target(ElementType.FIELD)
//@Retention(RetentionPolicy.RUNTIME)
//@Documented
public @interface FruitName {
    String value() default "";
}
