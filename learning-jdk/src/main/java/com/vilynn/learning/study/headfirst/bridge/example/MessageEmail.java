package com.vilynn.learning.study.headfirst.bridge.example;

/**
 * Created by weilin.wang on 2017/5/24
 */
public class MessageEmail implements MessageImplementor {

    @Override
    public void send(String message, String toUser) {
        System.out.println("使用E-mail的方式，发送消息:" + message + "给" + toUser);

    }

}
