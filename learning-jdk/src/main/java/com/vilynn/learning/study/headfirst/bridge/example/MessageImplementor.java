package com.vilynn.learning.study.headfirst.bridge.example;

/**
 * Created by weilin.wang on 2017/5/24
 */
/**
 * 实现发送消息的统一接口
 */
public interface MessageImplementor {

    /**
     * 发送消息
     *
     * @param message 消息的内容
     * @param toUser  消息的接收人
     */
    public void send(String message, String toUser);

}
