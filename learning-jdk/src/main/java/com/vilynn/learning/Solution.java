package com.vilynn.learning;

/**
 * Created by wangweilin on 2019/10/31.
 */
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public static int minDiffInBST(TreeNode root) {
        int c = 100;
        if(root == null){
            return c;
        }
        int l = 100;
        int r = 100;
        if(root.left != null){
            l = Math.abs(root.val - root.left.val);
        }
        if(root.right != null){
            r = Math.abs(root.val - root.right.val);
        }
        c = (l<r)?l:r;

        int ll = minDiffInBST(root.left);
        int rr = minDiffInBST(root.right);
        ll = ll<rr?ll:rr;

        return c<ll?c:ll;
    }

    public static class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
     }

    public static void main(String[] args) {
        TreeNode node = new TreeNode(90);
        node.left = new TreeNode(69);

        node.left.left = new TreeNode(49);
        node.left.right = new TreeNode(89);

        node.left.left.right = new TreeNode(52);

        System.out.println(minDiffInBST(node));
    }
}
