package com.vilynn.learning.workfile;

import com.alibaba.fastjson.JSON;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * WorkFile
 *
 * @author Weilin Wang
 * @since 2020/12/3
 */
public class WorkFile {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        File file = new File("E:\\work-notes\\nonobank");
        me(file, set);
        System.out.println(set.size());
        System.out.println(JSON.toJSONString(set));


        Set<String> set2 = new HashSet<>();
        File file2 = new File("E:\\Projects_own\\document");
        me(file2, set2);
        System.out.println(set2.size());
        System.out.println(JSON.toJSONString(set2));

        Iterator<String> iterator = set2.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            if (set.contains(next)) {
                iterator.remove();
            }
        }
        System.out.println(set2.size());
        System.out.println(JSON.toJSONString(set2));
    }

    public static void me(File file, Set<String> set){
        if (file.isDirectory()) {
            File[] listFiles = file.listFiles();
            for (File subFile : listFiles) {
                me(subFile, set);
            }
        } else {
            set.add(file.getName());
        }
    }
}
