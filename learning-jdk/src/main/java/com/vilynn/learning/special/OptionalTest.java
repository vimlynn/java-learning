package com.vilynn.learning.special;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by wangweilin on 2018/11/13.
 */
public class OptionalTest {
    @Test
    public void whenMap_thenOk() {
        User user = new User("anna@gmail.com", "1234");
        String email = Optional.ofNullable(user)
                .map(u -> u.getEmail()).orElse("default@gmail.com");

        assertEquals(email, user.getEmail());
    }

    @Test
    public void whenGetStream_thenOk() {
        User user = new User("john@gmail.com", "1234");
//        List<String> emails = Optional.ofNullable(user)
//                .stream()
//                .filter(u -> u.getEmail() != null && u.getEmail().contains("@"))
//                .map( u -> u.getEmail())
//                .collect(Collectors.toList());

//        assertTrue(emails.size() == 1);
//        assertEquals(emails.get(0), user.getEmail());
    }
}
