package com.vilynn.learning.special;

import lombok.Data;

/**
 * Created by wangweilin on 2018/11/13.
 */
@Data
public class User {
    private String name;
    private String email;

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }
}
