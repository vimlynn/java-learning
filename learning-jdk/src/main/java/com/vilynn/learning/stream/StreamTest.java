package com.vilynn.learning.stream;

import com.alibaba.fastjson.JSON;
import com.vilynn.learning.pojo.StoreGoods;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author weilin.wang
 */
public class StreamTest {

    @Test
    public void filter(){
        List<StoreGoods> list = initList();

        System.out.println(JSON.toJSONString(list));
        list = list.stream().filter((s) -> s.getId() > 3).collect(Collectors.toList());
        System.out.println(JSON.toJSONString(list));
    }

    @Test
    public void test(){
        List<StoreGoods> list = initList();

        System.out.println(JSON.toJSONString(list));
//        list = list.stream().anyMatch(() -> );
        System.out.println(JSON.toJSONString(list));
    }

    public List<StoreGoods> initList(){
        List<StoreGoods> list = new ArrayList<>();
        StoreGoods storeGoods = null;
        for (int i = 0; i < 5; i++) {
            storeGoods = new StoreGoods(i, "funny" + i);
            list.add(storeGoods);
        }
        return list;
    }
}
