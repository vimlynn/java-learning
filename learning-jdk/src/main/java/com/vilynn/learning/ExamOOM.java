package com.vilynn.learning;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wangweilin on 2019/9/30.
 */
public class ExamOOM {
    public static void main(String[] args) {
        List<User> list = new ArrayList<>();
        long s = System.currentTimeMillis();
        int n = 1000000;
        for (int i = 0; i < n; i++) {
            list.add(new User("名字"+i, 10+i));
        }
        long e = System.currentTimeMillis();
        System.out.println(e - s);
    }
    public static class User{
        private String name;
        private int age;

        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }
    }
}
