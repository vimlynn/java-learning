package com.vilynn.learning.number;

/**
 * @author weilin.wang
 */
public class NumberTest {
    public static void main(String[] args) {
        System.out.println(Long.toHexString(-6153345432608285575L));
//        System.out.println(Long.parseLong("aa9aecee4d1c5c79", 16));

        String[] charArray = "aa9aecee4d1c5c79".split("");
        String ph=String.valueOf(charArray[10])+String.valueOf(charArray

                [11])+String.valueOf(charArray[12])+String.valueOf(charArray[13]);
        double ph_d=0.0;

        int ph_t=Integer.parseInt(ph,16);
        String ph_bi=Integer.toBinaryString(ph_t);
        char[] ph_char=ph_bi.toCharArray();

        if(ph_t/100.0>513){
            String ph_bi2="";
            for(char obj:ph_char){
                if(obj=='1'){
                    ph_bi2+="0";
                }else{
                    ph_bi2+="1";
                }
            }
            ph_d=-(Integer.valueOf(ph_bi2,2)+1);
        }else{
            ph_d=ph_t/100.0;
        }
        System.out.println(ph_d);
    }
}
