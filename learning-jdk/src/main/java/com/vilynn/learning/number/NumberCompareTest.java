package com.vilynn.learning.number;

/**
 * @author weilin.wang
 * @version v1.0
 * @date 2018/9/11
 * @see
 * @since 1.0
 */
public class NumberCompareTest {
    public static void main(String[] args) {
        Double d1 = 12d;
        Double d2 = 12d;
        System.out.println(d1 == d2);

        String s1 = "ab".intern();
        String s = new String("ab");

//        String s2 = new String("ab");
        System.out.println(s == s1);
    }
}
