package com.vilynn.learning.thread;

/**
 * Created by weilin.wang on 2018/3/10
 */
public class TestThread3 implements Runnable {

    static int num;

    @Override
    public void run() {
//        num = num + 1;
        ++num;
        System.out.println("线程 ----------" + num);
    }

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new TestThread3());
        thread.start();

        System.out.println("++++++++" + num);
        new Thread(new TestThread3()).start();
        System.out.println("++++++++" + num);
        new Thread(new TestThread3()).start();
        System.out.println("++++++++" + num);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
//                    System.out.println("++++++++" + num);
                } catch (InterruptedException e) {

                }
            }
        }).start();
        Thread.sleep(1000);
        System.out.println("++++++++" + num);
    }

}