package com.vilynn.learning.thread;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by weilin.wang on 2018/3/3
 */
//@Slf4j
public class TestThread2 implements Runnable {

    private TestThread testThread;

    public TestThread2(TestThread testThread) {
        this.testThread = testThread;
    }

    @Override
    public void run() {
        synchronized (testThread) {
            testThread.notify();
        }
    }


}