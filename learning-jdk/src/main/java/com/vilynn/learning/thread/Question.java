package com.vilynn.learning.thread;

import java.util.Calendar;

/**
 * Created by weilin.wang on 2018/2/26
 */
public class Question {
    public static void main(String[] args) throws Exception {

        Thread.sleep(0);

        Thread.yield();


        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(0);
        System.out.println(calendar.toString());


        String s1 = "Programming";
        String s2 = new String("Programming");
        String s3 = "Program" + "ming";
        System.out.println(s1 == s2);
        System.out.println(s1 == s3);
        System.out.println(s1 == s1.intern());


        System.out.println("--------------start");
        A:for (int j = 0; j < 100; j++) {
            System.out.println("--------------j");
            B:for (int i = 0; i < 99; i++) {
                System.out.println("--------------i");
                if(i == 3){
                    break A;
                }
            }
        }
        System.out.println("--------------end");
    }
}
