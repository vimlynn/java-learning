package com.vilynn.learning.thread;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by weilin.wang on 2018/3/3
 */
//@Slf4j
public class TestThread implements Runnable {

    public static void main(String[] args) throws InterruptedException {
//        for (int i = 0; i < 1; i++) {
        Thread.currentThread().wait();
        TestThread testThread = new TestThread();
            Thread thread = new Thread(testThread);
            thread.start();
//        }
        new Thread(new TestThread2(testThread)).start();
    }

    @Override
    public void run() {
        System.out.println("--------开始：" + Thread.currentThread().getName());
        process();
        System.out.println("--------结束：" + Thread.currentThread().getName());
    }

    private synchronized void process() {
        try {
            System.out.println("--------wait开始：" + Thread.currentThread().getName());
            this.wait();

            System.out.println("--------wait结束：" + Thread.currentThread().getName());

//            System.out.println("--------notify开始：" + Thread.currentThread().getName());
//
//            this.notify();
//            System.out.println("--------notify结束：" + Thread.currentThread().getName());

        } catch (InterruptedException e) {
//            log.error("", e);
        }
    }
}