package com.vilynn.learning.thread;

/**
 * Created by weilin.wang on 2018/2/26
 */
public class A {

    public static void main(String[] args)
            throws Exception {
        try {
            try {
                throw new Sneeze();
            }
            catch ( Annoyance a ) {
                System.out.println("Caught Annoyance");
                throw a;
            }
        }
        catch ( Sneeze s ) {
            System.out.println("Caught Sneeze");
            return ;
        }
        finally {
            System.out.println("Hello World!");
        }
    }
}
    class Annoyance extends Exception {}
    class Sneeze extends Annoyance {}
