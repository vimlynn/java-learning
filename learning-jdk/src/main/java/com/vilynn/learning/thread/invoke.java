package com.vilynn.learning.thread;

import com.alibaba.fastjson.JSON;
import lombok.Data;

import java.lang.reflect.Field;

/**
 * Created by weilin.wang on 2018/3/7
 */
public class invoke {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        Test test = new Test();
        Class clazz = test.getClass();
//        clazz.getDeclaredAnnotationsByType();
        Field[] fields = clazz.getDeclaredFields();
        clazz.getAnnotationsByType(Runnable.class);
        for(Field field : fields){
            System.out.println(field.isAccessible());
//            field.setAccessible(true);
            if(field.getName().equals("s")){
                field.set(test, "1");
            }
        }
        System.out.println(JSON.toJSONString(test));
    }
}

@Data
class Test {
    private Boolean o;
    private String s;
    private Double d;
}
