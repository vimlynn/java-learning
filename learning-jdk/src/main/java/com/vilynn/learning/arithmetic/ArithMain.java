package com.vilynn.learning.arithmetic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by wangweilin on 2019/9/30.
 */
public class ArithMain {
    public static void main(String[] args) {

        System.out.println(Integer.MAX_VALUE);
        System.out.println(threeSumClosest(new int[]{-1,2,1,-4}, 1));

        int[] nums = new int[]{7,-1,14,-12,-8,7,2,-15,8,8,-8,-14,-4,-5,7,9,11,-4,-15,-6,1,-14,4,3,10,-5,2,1,6,11,2,-2,-5,-7,-6,2,-15,11,-6,8,-4,2,1,-1,4,-6,-15,1,5,-15,10,14,9,-8,-6,4,-6,11,12,-15,7,-1,-9,9,-1,0,-4,-1,-12,-2,14,-9,7,0,-3,-4,1,-2,12,14,-10,0,5,14,-1,14,3,8,10,-8,8,-5,-2,6,-11,12,13,-7,-12,8,6,-13,14,-2,-5,-11,1,3,-6};
//        System.out.println(threeSum(nums));

//        System.out.println(longestCommonPrefix(new String[]{"flight","flow","fl"}));
        List<Integer> l = Arrays.asList(1,2,3);
    }

    public static int threeSumClosest(int[] nums, int target) {
        int len = nums.length;
        int r=0;
        int count = 0;
        for(int i=0;i<len;i++){
            for(int j=0;j<len;j++){
                if(i == j){
                    break;
                }
                for(int k=0;k<len;k++){
                    if(j==k || i == k){
                        break;
                    }
                    int t=nums[i]+nums[j]+nums[k];
                    if(++count==1){
                        r=t;
                    }else{
                        int c1 = (Math.abs(target-r)>Math.abs(r-target))
                                ?Math.abs(r-target):Math.abs(target-r);
                        int c2 = (Math.abs(target-t)>Math.abs(t-target))
                                ?Math.abs(t-target):Math.abs(target-t);
                        r = (c1>c2)?t:r;
                    }
                }
            }
        }
        return r;
    }

    public static List<List<Integer>> threeSum(int[] nums) {
        if(nums == null || nums.length == 0){
            return null;
        }
        List<List<Integer>> l = new ArrayList<>();
        int len = nums.length;
        for(int i=0;i<len;i++){
            for(int j=0;j<len;j++){
                if(i == j){
                    break;
                }
                for(int k=0;k<len;k++){
                    if(j == k){
                        break;
                    }
                    if(i == k){
                        break;
                    }
                    if(nums[i]+nums[j]+nums[k] == 0){
                        l.add(Arrays.asList(nums[i],nums[j],nums[k]));
                    }
                }
            }
        }
        return l;
    }

    public static String longestCommonPrefix(String[] strs) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i<9999; i++){
            char p = '0';
            for(String s : strs){
                if(i >= s.length()){
                    return sb.toString();
                }
                char c = s.charAt(i);
                if(p == '0'){
                    p = c;
                }
                if(c != p){
                    return sb.toString();
                }
            }
            sb.append(p);
        }
        return sb.toString();
    }
}
