package com.vilynn.learning.arithmetic;

/**
 * A
 *
 * @author weilin
 * @since 2021/1/20
 */
public class A {
    public static void main(String[] args) {
        String str1 = "5f20f60a6c62297a89867d85";
        String str2 = "5f20f60a6c62297a89867d86";
        byte[] array1 = str1.getBytes();
        byte[] array2 = str2.getBytes();
        int length = array1.length;
        byte[] array3 = new byte[length];
        for (int i = 0; i < length; i++) {
            array3[i] = (byte) (array1[i] ^ array2[i]);
        }
        System.out.println(new String(array3));
    }
}
