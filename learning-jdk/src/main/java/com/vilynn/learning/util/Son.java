package com.vilynn.learning.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * Created by weilin.wang on 2018/4/3
 */
@Data
@ToString(callSuper = true)
@AllArgsConstructor
public class Son extends Parent {
    private String name;
}
