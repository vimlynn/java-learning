package com.vilynn.learning.util;

import lombok.Data;

/**
 * Created by weilin.wang on 2018/4/3
 */
@Data
public class Parent {
    private Object obj;
}
