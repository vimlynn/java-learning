package com.vilynn.learning.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by weilin.wang on 2018/4/3
 */
class HashMapLearn {

    private native void test();

    static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    public static void main(String[] args) {
        for (int i = 0; i < 20; i++) {
            System.out.println(hash(new Object()));
        }
        Map<Integer, Son> map1 = new HashMap<>();
        map1.put(1, new Son("wwl"));

        Map<Integer, Parent> map2 = new HashMap<>(map1);
        System.out.println(JSON.toJSONString(map2, SerializerFeature.WriteMapNullValue));
    }

}
