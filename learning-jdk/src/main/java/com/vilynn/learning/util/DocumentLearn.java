package com.vilynn.learning.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by weilin.wang on 2018/4/3
 */
public class DocumentLearn {
    public static void main(String[] args) {
        Document document = new Document("dom");
        document.setList(new ArrayList<>());

        document.getList().add(new Document("list1"));

        List<Document> innerList = new ArrayList<>();
        innerList.add(new Document("inner1", "inSpan"));
        innerList.add(new Document("inner2"));
        document.getList().get(0).setList(innerList);

        document.getList().add(new Document("list2"));

        innerList.remove(1);
        document.getList().get(1).setList(innerList);

        document.setValue("span");
        document.print();
    }
}
