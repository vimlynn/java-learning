package com.vilynn.learning.util;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by weilin.wang on 2018/4/3
 */
@Data
@NoArgsConstructor
public class Document {

    private List<Document> list;
    private String name;
    private String value;

    private static int level;

    public Document(String name) {
        this.name = name;
    }

    public Document(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public void print(){
        if(name != null){
            System.out.println(getStr(name, level++));
        }
        if(list != null){
            for(Document document : list){
                document.print();
            }
        }
        if(value != null){
            System.out.print(getStr(value, level));
            System.out.println(getStr2(value, 0));
        }
        if(name != null) {
            System.out.println(getStr2(name, --level));
        }
    }


    private String getStr(String str, int level){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < level; i++) {
            sb.append("  ");
        }
        return sb.toString() + "<" +str+ ">";
    }

    private String getStr2(String str, int level){
        return getStr(str, level).replace("<", "</");
    }

}