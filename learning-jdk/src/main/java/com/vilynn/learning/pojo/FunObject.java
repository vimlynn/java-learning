package com.vilynn.learning.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author weilin.wang
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FunObject {
    private int age;
    private String name;
    private boolean isSx;
    private FunObject funObject;

    public FunObject(int age) {
        this.age = age;
    }

    public void method(){
        System.out.println("这是一个对象的方法");
    }
}
