package com.vilynn.learning.pojo;

import lombok.Data;

/**
 * @author weilin.wang
 */
@Data
public class StoreGoods {
    private int id;
    private String name;
    private String picUrl;
    private String tag;

    public StoreGoods(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
