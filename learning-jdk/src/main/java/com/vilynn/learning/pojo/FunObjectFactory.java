package com.vilynn.learning.pojo;

/**
 * @author weilin.wang
 */
public class FunObjectFactory {
    private static volatile int index = 0;
    public static FunObject getBean(){
        return new FunObject(index++);
    }
}
