package com.vilynn.learning.exec;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by wangweilin on 2019/2/13.
 */
public class ExecuteFrame extends Frame {

    public ExecuteFrame(){
        this.setBounds(100,100,1000,1000);
        this.setTitle("开奖啦～～～");
        this.setAlwaysOnTop(true);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(1000 * 60 * 10);
        new ExecuteFrame().show();
    }
}
