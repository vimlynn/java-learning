package com.vilynn.learning.new2.designmode.adapter;

import com.vilynn.learning.new2.designmode.adapter.AbstractAdapter;

/**
 * Adapter
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class Adapter implements AbstractAdapter {

    private AbstractAdapter adapter;

    public Adapter(AbstractAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public void push() {
        adapter.push();
    }
}
