package com.vilynn.learning.new2.designmode.adapter;

/**
 * PxqAdapter
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class PxqAdapter implements AbstractAdapter {
    @Override
    public void push() {
        System.out.println("票星球推送数据");
    }
}
