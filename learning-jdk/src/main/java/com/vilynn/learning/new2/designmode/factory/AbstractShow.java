package com.vilynn.learning.new2.designmode.factory;

/**
 * AbstractShow
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public interface AbstractShow {
    void show();
}
