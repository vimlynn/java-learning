package com.vilynn.learning.new2.basics;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

/**
 * ForkJoin
 *
 * @author Weilin Wang
 * @since 2020/10/15
 */
public class ForkJoin {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

//        ForkJoinTask<String> task = new MyForkJoinTask();
//        task.fork();
//        ForkJoinTask<String> task2 = new MyForkJoinTask2();
//        task2.fork();
//
//        String s = task.join() + task2.join();
//        System.out.println(s);

        long start = System.currentTimeMillis();

        ForkJoinPool pool = new ForkJoinPool(1);
        ForkJoinTask<String> submit = pool.submit(() -> {
            Thread.sleep(1000L);
            return "查询优惠券";
        });
        ForkJoinTask<String> submit1 = pool.submit(() -> {
            Thread.sleep(1000L);
            return "查询会员卡";
        });

        submit1.fork();
        String s1 = submit.join() + submit1.join();

        long end = System.currentTimeMillis();
        System.out.println(s1 + "   cos time:" + (end-start));

    }


    private static class MyForkJoinTask extends ForkJoinTask<String> {

        @Override
        public String getRawResult() {
            return "查询优惠券结果";
        }

        @Override
        protected void setRawResult(String value) {

        }

        @Override
        protected boolean exec() {
            return false;
        }
    }

    private static class MyForkJoinTask2 extends ForkJoinTask<String> {

        @Override
        public String getRawResult() {
            return "查询会员卡结果";
        }

        @Override
        protected void setRawResult(String value) {

        }

        @Override
        protected boolean exec() {
            return false;
        }
    }
}
