package com.vilynn.learning.new2.designmode.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Subject
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class Subject implements Event {

    private List<Observer> observers = new ArrayList<>();

    public void add(Observer observer){
        observers.add(observer);
    }

    public void remove(Observer observer){
        observers.remove(observer);
    }

    public void change(){
        System.out.println("主题变了");
        for (Observer observer : observers) {
            observer.change();
        }
    }
}
