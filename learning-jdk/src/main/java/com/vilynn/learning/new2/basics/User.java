package com.vilynn.learning.new2.basics;

import lombok.Data;

import java.util.Arrays;
import java.util.List;

/**
 * User
 *
 * @author Weilin Wang
 * @since 2020/8/24
 */
@Data
public class User {
    private String name;
    private Integer age;
    private Boolean sex;
    private UserInfo userInfo;
    private List<String> tags;
    private List<UserInfo> userInfos;
    private List<Integer> ints;

    public User(String name, List<String> tags) {
        this.name = name;
        this.tags = tags;
    }

    public static List<User> list(){
        User user1 = new User("ww", Arrays.asList("地痞", "流氓"));
        User user2 = new User("wwl", Arrays.asList("三好学生", "优秀"));

        user1.setUserInfos(Arrays.asList(new UserInfo("1", "2", "3"),
                new UserInfo("2-1", "2-2", "3-3")
                ));
        return Arrays.asList(user1, user2);
    }
}
