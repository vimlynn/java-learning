package com.vilynn.learning.new2.designmode.observer;

/**
 * Event
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public interface Event {
    void change();
}
