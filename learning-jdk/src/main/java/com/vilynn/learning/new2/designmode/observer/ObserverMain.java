package com.vilynn.learning.new2.designmode.observer;

/**
 * ObserverMain
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class ObserverMain {
    public static void main(String[] args) {
        Subject subject = new Subject();
        subject.add(new Observer());
        subject.add(new Observer());

        subject.change();
    }
}
