package com.vilynn.learning.new2.designmode.factory;

/**
 * PxqVenue
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class PxqVenue implements AbstractVenue {
    @Override
    public void open() {
        System.out.println("票星球，开场了。");
    }
}
