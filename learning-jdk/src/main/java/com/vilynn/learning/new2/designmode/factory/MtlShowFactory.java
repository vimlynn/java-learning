package com.vilynn.learning.new2.designmode.factory;

/**
 * MtlShowFactory
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class MtlShowFactory implements AbstractShowFactory, AbstractFactory {
    @Override
    public AbstractVenue createVenue() {
        return new MtlVenue();
    }

    @Override
    public AbstractShow createShow() {
        return new MtlShow();
    }
}
