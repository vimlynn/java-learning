package com.vilynn.learning.new2.designmode.adapter;

/**
 * AbstractAdapter
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public interface AbstractAdapter {
    void push();
}
