package com.vilynn.learning.new2.basics;

import java.util.Collections;
import java.util.List;

/**
 * CollectionsMain
 *
 * @author Weilin Wang
 * @since 2020/8/25
 */
public class CollectionsMain {
    public static void main(String[] args) {
        List<User> list = User.list();

        List<Object> objects = Collections.emptyList();
        objects.addAll(list);
    }
}
