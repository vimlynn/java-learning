package com.vilynn.learning.new2.question;

/**
 * StringInverse
 *
 * @author Weilin Wang
 * @since 2020/4/15
 */
public class StringInverse {

    public static void main(String[] args) {
        String s = "WiUHUYyuvUYVyTYGUUGYyG788u";
        System.out.println(inverse(s));
    }

    public static String inverse(String s) {
        StringBuilder sb = new StringBuilder();
        int length = s.length();
        for (int i = 0; i < length; i++) {
            char c = s.charAt(length - 1 - i);
            if(i == 0 || Character.isLowerCase(c)){
                c = Character.toUpperCase(c);
            }else {
                c = Character.toLowerCase(c);
            }
            sb.append(c);
        }
        return sb.toString();
    }

}
