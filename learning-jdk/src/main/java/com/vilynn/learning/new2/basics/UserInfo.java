package com.vilynn.learning.new2.basics;

import lombok.Data;

/**
 * UserInfo
 *
 * @author Weilin Wang
 * @since 2020/8/24
 */
@Data
public class UserInfo {
    private String province;
    private String city;
    private String district;

    public UserInfo(String province, String city, String district) {
        this.province = province;
        this.city = city;
        this.district = district;
    }
}
