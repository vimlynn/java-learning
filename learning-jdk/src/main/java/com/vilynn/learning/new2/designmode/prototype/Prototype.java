package com.vilynn.learning.new2.designmode.prototype;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * Prototype
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
@Slf4j
@Data
public class Prototype implements Cloneable {
    private String name;
    private Integer number;
    private boolean show;
    private Map<String, Prototype> map;

    @Override
    protected Prototype clone() {
        Prototype prototype = null;

        try {
            prototype = (Prototype) super.clone();
            prototype.map = (Map<String, Prototype>)((HashMap) this.map).clone();

        } catch (CloneNotSupportedException e) {
            log.error("CloneNotSupportedException");
        }
        return prototype;
    }

    public static void main(String[] args) {
        Prototype prototype = new Prototype();
        prototype.setName("王维林");
        prototype.setNumber(11);
        prototype.setShow(true);


        Map<String, Prototype> map2 = new HashMap<>();
        Prototype prototype2 = new Prototype();
        prototype2.setName("王维林");
        prototype2.setNumber(11);
        prototype2.setShow(true);
        map2.put("1", prototype2);

        prototype.setMap(map2);

        System.out.println(JSON.toJSONString(prototype));
        Prototype clone = prototype.clone();
        System.out.println(JSON.toJSONString(clone));

    }
}
