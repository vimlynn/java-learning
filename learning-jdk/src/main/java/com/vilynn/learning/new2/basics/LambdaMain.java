package com.vilynn.learning.new2.basics;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * LamdbaMain
 *
 * @author Weilin Wang
 * @since 2020/8/24
 */
public class LambdaMain {

    public static void main(String[] args) {
        User user1 = new User("ww", Arrays.asList("地痞", "流氓"));
        User user2 = new User("wwl", Arrays.asList("三好学生", "优秀"));

        List<User> userList = new ArrayList<>();
        userList.add(user1);
        userList.add(user2);

        List<String> collect = userList.stream().map(User::getTags).collect(Collectors.collectingAndThen(Collectors.toList(), v -> v.get(0)));
        System.out.println(collect);

        List<String> collect1 = userList.stream().map(User::getTags).map(Objects::toString).collect(Collectors.toList());
        System.out.println(collect1);

        List<String> collect2 = userList.stream().flatMap(u -> u.getTags().stream()).collect(Collectors.toList());
        System.out.println(collect2);

        Set<String> collect3 = userList.parallelStream().flatMap(u -> u.getTags().stream()).collect(Collectors.toSet());
        System.out.println(collect3);

        userList.stream().map(User::getInts).mapToInt(List::size).sum();

        Stream.generate(new Random()::nextInt).limit(10).forEach(i -> System.out.println(i));

        try (InputStream is = new FileInputStream("")) {
            for (int i = 0; i < is.available(); i++) {

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
