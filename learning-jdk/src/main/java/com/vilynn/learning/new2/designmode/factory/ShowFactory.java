package com.vilynn.learning.new2.designmode.factory;

/**
 * ShowFactory
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class ShowFactory {
    public static AbstractShow newShow(String type){
        switch (type) {
            case "pxq": return new PxqShow();
            case "mtl": return new MtlShow();
        }
        return new AbstractShow() {
            @Override
            public void show() {
                System.out.println("抽象演出");
            }
        };
    }
}
