package com.vilynn.learning.new2.designmode.factory;

/**
 * PxqShowFactory
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class PxqShowFactory implements AbstractShowFactory, AbstractFactory {
    @Override
    public AbstractVenue createVenue() {
        return new PxqVenue();
    }

    @Override
    public AbstractShow createShow() {
        return new PxqShow();
    }
}
