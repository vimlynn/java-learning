package com.vilynn.learning.new2.designmode.factory;

/**
 * MtlShow
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class MtlShow implements AbstractShow {
    @Override
    public void show() {
        System.out.println("摩天轮开演了");
    }
}
