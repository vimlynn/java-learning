package com.vilynn.learning.new2.sort;

import com.alibaba.fastjson.JSON;

/**
 * SortMain
 *
 * @author Weilin Wang
 * @since 2020/8/6
 */
public class SortMain {
    public static void main(String[] args) {
        int[] array = new int[]{-1,3,7,2,-35,72};
        System.out.println(JSON.toJSONString(array));
//        mao(array);
//        select(array);
//        insert(array);
        quick(array);
        System.out.println(JSON.toJSONString(array));
    }

    private static void quick(int[] array) {
        quick(array, 0, array.length -1);
    }

    private static void quick(int[] array, int low, int high) {
        int temp = array[low];
        while (true) {
            if (low >= high) {
                break;
            }
            while (true) {
                if (array[high] < temp) {
                    array[low] = array[high];
                    break;
                }
                high--;
            }
            while (true) {
                if (array[++low] > temp) {
                    array[high] = array[low];
                    break;
                }
            }
        }

    }

    private static void insert(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int pos = i;
            for (int j = i - 1; j >= 0; j--) {
                if (array[i] < array[j]) {
                    pos = j;
                } else {
                    break;
                }
            }
            for (int k = i; k > pos; k--) {
                array[k] = array[k-1];
            }
            array[pos] = array[i];
        }
    }

    private static void select(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int temp = array[i];
            int pos = i;
            for (int j = i; j < array.length; j++) {
                if (array[j] < temp) {
                    temp = array[j];
                    pos = j;
                }
            }
            array[pos] = array[i];
            array[i] = temp;
        }
    }

    private static void mao(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i+1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
    }
}
