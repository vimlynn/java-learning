package com.vilynn.learning.new2.designmode.factory;

/**
 * AbstractShowFactory
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public interface AbstractShowFactory {
    AbstractShow createShow();
}
