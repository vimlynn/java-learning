package com.vilynn.learning.new2.basics;

import com.alibaba.fastjson.JSON;

import java.util.List;

/**
 * ReferenceMain
 *
 * @author Weilin Wang
 * @since 2020/8/26
 */
public class ReferenceMain {
    public static void main(String[] args) {
        List<User> userList = User.list();
        System.out.println(JSON.toJSONString(userList));
        for (User user : userList) {
            chet(user.getUserInfos());
        }
        System.out.println(JSON.toJSONString(userList));
    }

    private static void chet(List<UserInfo> userInfos) {
        if (userInfos == null) {
            return;
        }
        userInfos.get(0).setProvince("陕西省");
    }
}
