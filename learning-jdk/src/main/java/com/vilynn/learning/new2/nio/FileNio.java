package com.vilynn.learning.new2.nio;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * FileNio
 *
 * @author Weilin Wang
 * @since 2020/4/17
 */
public class FileNio {
    public static void main(String[] args) throws IOException {
        try (
        FileChannel fileChannelIn = new FileInputStream("").getChannel();
        FileChannel fileChannelOut = new FileOutputStream("").getChannel();
        ) {
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
            while (fileChannelIn.read(byteBuffer) != -1) {
                byteBuffer.flip();
                fileChannelOut.write(byteBuffer);
                byteBuffer.clear();
            }
        }
    }
}
