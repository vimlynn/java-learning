package com.vilynn.learning.new2.question;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Lamdba
 *
 * @author Weilin Wang
 * @since 2020/4/26
 */
public class Lamdba {
    public static void main(String[] args) {
        List<Name> list1 = new ArrayList<>();
        list1.add(new Name("1", 1));
        list1.add(new Name("2", 1));
        list1.add(new Name("3", 1));
        list1.add(new Name("4", null));

        Map<String, List<Name>> listMap = list1.stream().collect(Collectors.groupingBy(Name::getA));

        /**
         * Exception in thread "main" java.lang.IllegalStateException: Duplicate key Lamdba.Name(a=1, b=1)
         * 	at java.util.stream.Collectors.lambda$throwingMerger$0(Collectors.java:133)
         * 	at java.util.HashMap.merge(HashMap.java:1254)
         * 	at java.util.stream.Collectors.lambda$toMap$58(Collectors.java:1320)
         * 	at java.util.stream.ReduceOps$3ReducingSink.accept(ReduceOps.java:169)
         * 	at java.util.ArrayList$ArrayListSpliterator.forEachRemaining(ArrayList.java:1382)
         * 	at java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:481)
         * 	at java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:471)
         * 	at java.util.stream.ReduceOps$ReduceOp.evaluateSequential(ReduceOps.java:708)
         * 	at java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
         * 	at java.util.stream.ReferencePipeline.collect(ReferencePipeline.java:499)
         * 	at Lamdba.main(Lamdba.java:24)
         */
        Map<String, Name> nameMap = list1.stream().collect(Collectors.toMap(Name::getA, Function.identity()));
    }

    @Data
    @AllArgsConstructor
    private static class Name {
        private String a;
        private Integer b;
    }
}
