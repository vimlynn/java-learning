package com.vilynn.learning.new2.designmode.adapter;

/**
 * AdapterMain
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class AdapterMain {

    public static void main(String[] args) {
        Adapter adapter = new Adapter(new com.vilynn.learning.new2.designmode.adapter.MtlAdapter());
        adapter.push();

        Adapter adapter2 = new Adapter(new com.vilynn.learning.new2.designmode.adapter.PxqAdapter());
        adapter2.push();
    }
}
