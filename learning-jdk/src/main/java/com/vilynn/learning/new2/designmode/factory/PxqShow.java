package com.vilynn.learning.new2.designmode.factory;

/**
 * PxqShow
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class PxqShow implements AbstractShow {
    @Override
    public void show() {
        System.out.println("票星球开演了");
    }
}
