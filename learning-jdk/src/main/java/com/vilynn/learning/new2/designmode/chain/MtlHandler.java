package com.vilynn.learning.new2.designmode.chain;

import lombok.Data;

/**
 * MtlHandler
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
@Data
public class MtlHandler implements Handler {

    private Handler next;

    @Override
    public void handle() {
        System.out.println("摩天轮处理器");
        if (next != null) {
            next.handle();
        }
    }
}
