package com.vilynn.learning.new2.question;

/**
 * ThreePart
 *
 * @author Weilin Wang
 * @since 2020/4/23
 */
public class ThreePart {

    public static void main(String[] args) {
        int[] A = new int[]{0,2,1,-6,6,-7,9,1,2,0,1};
        boolean res = canThreePartsEqualSum(A);
        System.out.println(res);
    }

    public static boolean canThreePartsEqualSum(int[] A) {
        int sum = 0;
        for(int a : A){
            sum += a;
        }
        int index = 0;
        int sumA = 0;
        for(int i = 0; i < A.length; i++){
            sumA += A[i];
            if(sum == sumA*3){
                sumA = 0;
                index++;
            }
            if(index == 3 && i == A.length-1){
                return true;
            }
        }
        return false;
    }

}
