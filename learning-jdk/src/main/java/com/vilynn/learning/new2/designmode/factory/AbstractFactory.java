package com.vilynn.learning.new2.designmode.factory;

/**
 * AbstractFactory
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public interface AbstractFactory {
    AbstractVenue createVenue();
    AbstractShow createShow();
}
