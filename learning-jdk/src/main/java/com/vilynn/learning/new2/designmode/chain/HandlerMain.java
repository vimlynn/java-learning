package com.vilynn.learning.new2.designmode.chain;

/**
 * HandlerMain
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class HandlerMain {

    public static void main(String[] args) {
        Handler handler = new MtlHandler();
        handler.setNext(new PxqHandler());

        handler.handle();
    }
}
