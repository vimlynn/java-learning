package com.vilynn.learning.new2.designmode.factory;

/**
 * AbstractVenue
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public interface AbstractVenue {
    void open();
}
