package com.vilynn.learning.new2.question;

import java.util.ArrayList;
import java.util.List;

/**
 * QueueQuestion
 *
 * @author Weilin Wang
 * @since 2020/4/15
 */
public class QueueQuestion {
    private List q;
    public QueueQuestion(){
        q = new ArrayList();
    }

    public Object pop(){
        int size = q.size();
        Object o = q.get(size - 1);
        q.remove(o);
        return o;
    }

    public Object push(Object o){
        q.add(o);
        return o;
    }

    public static void main(String[] args) {
        QueueQuestion queue = new QueueQuestion();
        queue.push("1");
        queue.push("2");
        queue.push("3");

        System.out.println(queue.pop());
        System.out.println(queue.pop());

        queue.push("4");
        System.out.println(queue.pop());
        System.out.println(queue.pop());
    }
}
