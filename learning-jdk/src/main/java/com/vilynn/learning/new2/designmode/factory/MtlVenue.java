package com.vilynn.learning.new2.designmode.factory;

/**
 * MtlVenue
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class MtlVenue implements AbstractVenue {
    @Override
    public void open() {
        System.out.println("摩天轮，开场了。");
    }
}
