package com.vilynn.learning.new2.designmode.adapter;

/**
 * MtlAdapter
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class MtlAdapter implements AbstractAdapter {
    @Override
    public void push() {
        System.out.println("摩天轮推送数据");
    }
}
