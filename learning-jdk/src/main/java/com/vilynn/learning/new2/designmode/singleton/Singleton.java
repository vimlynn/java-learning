package com.vilynn.learning.new2.designmode.singleton;

/**
 * Singleton
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class Singleton {

    private Singleton(){

    }

    public static Singleton instance(){
        return new Singleton();
    }

    public static synchronized Singleton instance2(){
        return new Singleton();
    }

}
