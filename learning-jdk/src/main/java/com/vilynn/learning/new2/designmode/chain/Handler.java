package com.vilynn.learning.new2.designmode.chain;

/**
 * Handler
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public interface Handler {
    void setNext(Handler handler);
    Handler getNext();
    void handle();
}
