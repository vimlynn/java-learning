package com.vilynn.learning.new2.designmode.chain;

import lombok.Data;

/**
 * ResponsibilityChain
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
@Data
public class PxqHandler implements Handler {

    private Handler next;

    public void handle(){
        System.out.println("票星球处理");
        if (next != null) {
            next.handle();
        }
    }

}
