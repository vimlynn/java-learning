package com.vilynn.learning.new2.designmode.factory;

/**
 * FactoryMain
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class FactoryMain {

    public static void main(String[] args) {
        System.out.println("-----------简单工厂-----------");
        AbstractShow show = ShowFactory.newShow("pxq");
        show.show();

        AbstractShow show2 = ShowFactory.newShow("mtl");
        show2.show();


        System.out.println("-----------工厂方法-----------");
        AbstractShowFactory showFactory = new MtlShowFactory();
        showFactory.createShow().show();

        AbstractShowFactory showFactory2 = new PxqShowFactory();
        showFactory2.createShow().show();

        System.out.println("-----------抽象工厂-----------");
        AbstractFactory factory = new PxqShowFactory();
        AbstractShow show1 = factory.createShow();
        AbstractVenue venue1 = factory.createVenue();
        venue1.open();
        show1.show();
    }
}
