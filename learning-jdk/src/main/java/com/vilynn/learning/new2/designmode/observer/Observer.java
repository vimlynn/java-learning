package com.vilynn.learning.new2.designmode.observer;

/**
 * Observer
 *
 * @author Weilin Wang
 * @since 2020/8/20
 */
public class Observer implements Event {
    @Override
    public void change() {
        System.out.println("我也该变一变了。");
    }
}
