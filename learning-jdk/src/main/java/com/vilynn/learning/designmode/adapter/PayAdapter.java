package com.vilynn.learning.designmode.adapter;

/**
 * Created by wangweilin on 2019/10/23.
 */
public class PayAdapter implements PayService {

    private PayDemo payDemo;

    @Override
    public void pay() {
        payDemo.test();
    }
}
