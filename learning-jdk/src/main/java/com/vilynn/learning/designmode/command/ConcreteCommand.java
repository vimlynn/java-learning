package com.vilynn.learning.designmode.command;

import lombok.Data;

/**
 * Created by wangweilin on 2019/10/25.
 */
@Data
public class ConcreteCommand implements Command {

    private Receiver receiver;

    public ConcreteCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        receiver.action();
    }
}
