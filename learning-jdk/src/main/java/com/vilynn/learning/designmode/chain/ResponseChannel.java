package com.vilynn.learning.designmode.chain;

import lombok.Data;

/**
 * Created by wangweilin on 2018/11/26.
 */
@Data
public class ResponseChannel implements Channel {
    private Channel channel;
    @Override
    public Result send() {
        System.out.println("=== ResponseChannel start");
        Result res = channel.send();
        if("success".equals(res.getRes())){
            res.setCode("000000");
        }
        System.out.println("=== ResponseChannel end");
        return res;
    }
}
