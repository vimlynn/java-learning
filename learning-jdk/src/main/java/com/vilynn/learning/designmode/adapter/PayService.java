package com.vilynn.learning.designmode.adapter;

/**
 * Created by wangweilin on 2019/10/23.
 */
public interface PayService {
    void pay();
}
