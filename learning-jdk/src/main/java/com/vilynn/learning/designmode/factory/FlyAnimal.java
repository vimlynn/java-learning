package com.vilynn.learning.designmode.factory;

/**
 * Created by wangweilin on 2019/10/23.
 */
public interface FlyAnimal extends Animal {
    void fly();
}
