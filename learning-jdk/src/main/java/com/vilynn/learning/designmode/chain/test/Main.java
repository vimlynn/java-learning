package com.vilynn.learning.designmode.chain.test;

import com.vilynn.learning.designmode.chain.*;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Weilin on 2018/7/2.
 *
 * 责任链模式，保证每一条链子的逻辑都会被执行
 */
public class Main {
    public static void main(String[] args) {
        Learn learn = new Learn();
        Print print = new Print();
        Calculate calculate = new Calculate();
        List<Interceptor> list = Arrays.asList(print, calculate, learn, learn, calculate);

        InterceptorExecution execution = new InterceptorExecution();
        execution.setIterator(list.iterator());

        execution.execute();

    }
}
