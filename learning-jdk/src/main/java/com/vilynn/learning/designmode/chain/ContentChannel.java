package com.vilynn.learning.designmode.chain;

import lombok.Data;

/**
 * Created by wangweilin on 2018/11/26.
 */
@Data
public class ContentChannel implements Channel {
    private Channel channel;
    @Override
    public Result send() {
        System.out.println("===ContentChannel start");
        Result res = channel.send();
        System.out.println("===ContentChannel end");
        return res;
    }
}
