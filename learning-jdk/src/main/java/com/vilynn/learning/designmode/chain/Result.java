package com.vilynn.learning.designmode.chain;

import lombok.Data;

/**
 * Created by wangweilin on 2018/11/26.
 */
@Data
public class Result {
    private String res;
    private String code;
}
