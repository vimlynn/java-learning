package com.vilynn.learning.designmode.factory;

/**
 * Created by wangweilin on 2019/10/23.
 */
public class Chicken implements FlyAnimal {
    @Override
    public void eat() {
        System.out.println("鸡吃玉米");
    }

    @Override
    public void fly() {
        System.out.println("鸡飞起来了");
    }
}
