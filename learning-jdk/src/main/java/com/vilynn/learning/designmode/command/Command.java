package com.vilynn.learning.designmode.command;

/**
 * Created by wangweilin on 2019/10/25.
 */
public interface Command {
    void execute();
}
