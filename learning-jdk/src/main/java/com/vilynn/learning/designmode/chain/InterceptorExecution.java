package com.vilynn.learning.designmode.chain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.xml.ws.Response;
import java.util.Iterator;

/**
 * Created by Weilin on 2018/7/2.
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class InterceptorExecution implements Interceptor {
    private Iterator<Interceptor> iterator;
    @Override
    public Response execute(InterceptorExecution execution) {
        if(iterator.hasNext()){
            return iterator.next().execute(execution);
        }
        return null;
    }

    public void setIterator(Iterator<Interceptor> iterator) {
        this.iterator = iterator;
    }

    public Response execute() {
        InterceptorExecution execution = new InterceptorExecution();
        execution.setIterator(this.iterator);
        return execution.execute(this);
    }
}
