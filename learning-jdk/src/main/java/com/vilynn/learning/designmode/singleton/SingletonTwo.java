package com.vilynn.learning.designmode.singleton;

/**
 * Created by wangweilin on 2019/4/25.
 */
public class SingletonTwo {
    private SingletonTwo singletonOne;
    private SingletonTwo(){

    }
    public static SingletonTwo getInstance(){
        return SingletonOneHolder.instance;
    }
    private static class SingletonOneHolder{
        private static SingletonTwo instance = new SingletonTwo();
    }
}
