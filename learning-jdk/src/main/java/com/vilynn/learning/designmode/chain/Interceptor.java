package com.vilynn.learning.designmode.chain;

import javax.xml.ws.Response;

/**
 * Created by Weilin on 2018/7/2.
 */
public interface Interceptor {
    Response execute(InterceptorExecution execution);
}
