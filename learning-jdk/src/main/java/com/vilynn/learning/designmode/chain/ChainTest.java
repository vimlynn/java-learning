package com.vilynn.learning.designmode.chain;

/**
 * Created by wangweilin on 2018/11/26.
 */
public class ChainTest {
    public static void main(String[] args) {


        Channel headerChannel = new HeaderChannel();
        Channel contentChannel = new ContentChannel();
        Channel channel = new RealChannel();
        Channel responseChannel = new ResponseChannel();

        headerChannel.setChannel(contentChannel);
        contentChannel.setChannel(channel);
        channel.setChannel(responseChannel);

        Result result = headerChannel.send();
        System.out.println("res::::::" + result);


    }
}
