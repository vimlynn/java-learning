package com.vilynn.learning.designmode.singleton;

/**
 * Created by wangweilin on 2019/4/25.
 */
public class SingletonOne {
    private static volatile SingletonOne singletonOne;
    private SingletonOne(){

    }
    public static SingletonOne getInstance() {
        if (singletonOne == null) {
            synchronized (SingletonOne.class) {
                if (singletonOne == null){
                    singletonOne = new SingletonOne();
                }
            }
        }
        return singletonOne;
    }
}
