package com.vilynn.learning.designmode.factory;

/**
 * Created by wangweilin on 2019/10/23.
 */
public class Test {
    public static void main(String[] args) {
        Animal animal1 = AnimalFactory.create(1);

        Factory f = new CatFactory();
        Animal animal2 = f.create();



    }
}
