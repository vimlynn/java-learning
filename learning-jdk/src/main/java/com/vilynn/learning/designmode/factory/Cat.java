package com.vilynn.learning.designmode.factory;

/**
 * Created by wangweilin on 2019/10/23.
 */
public class Cat implements WalkAnimal {
    @Override
    public void eat() {
        System.out.println("猫吃鱼");
    }

    @Override
    public void walk() {
        System.out.println("猫在狂奔");
    }
}
