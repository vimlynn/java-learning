package com.vilynn.learning.designmode.strategy;

/**
 * @author weilin.wang
 */
public class Context {
    private Strategy strategy;
    public void contextInterface(){
        strategy.strategyInterface();
    }
}
