package com.vilynn.learning.designmode.command;

/**
 * Created by wangweilin on 2019/10/25.
 */
public class CommandClient {
    public static void main(String[] args) {
        Receiver receiver = new Receiver();
        Command command = new ConcreteCommand(receiver);
        Invoker invoker = new Invoker(command);
        invoker.action();
    }
}
