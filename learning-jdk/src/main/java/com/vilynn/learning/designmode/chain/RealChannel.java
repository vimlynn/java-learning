package com.vilynn.learning.designmode.chain;

import lombok.Data;

/**
 * Created by wangweilin on 2018/11/26.
 */
@Data
public class RealChannel implements Channel {
    private Channel channel;
    @Override
    public Result send() {
        System.out.println("===RealChannel start");
        Result res = new Result();
        res.setRes("--------success-------");
        System.out.println("===RealChannel end");
        return res;
    }
}
