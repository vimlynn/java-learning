package com.vilynn.learning.designmode.factory;

/**
 * Created by wangweilin on 2019/10/23.
 */
public class AnimalFactory {
    public static Animal create(int type){
        switch (type) {
            case 1:
                return new Cat();
            case 2:
                return new Chicken();
        }
        return null;
    }
}
