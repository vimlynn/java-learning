package com.vilynn.learning.designmode.chain;

import javax.xml.ws.Response;

/**
 * Created by Weilin on 2018/7/2.
 */
public class Print implements Interceptor {
    @Override
    public Response execute(InterceptorExecution execution) {
        System.out.println("da yin");
        return execution.execute();
    }
}
