package com.vilynn.learning.designmode.observer;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by wangweilin on 2018/11/20.
 */
public class Event {

    private final EventSource source;

    public Event() {
        this.source = new EventSource();
    }

    public static int getA(Set<String> list){
        return 0;
    }

    public static String getA(Collection<Integer> list){
        return "1";
    }

    public static String getA(List<Integer> list){
        return "2";
    }

    public static void main(String[] args) {
        List<Integer> l = Arrays.asList(1,2);
        getA(l);

        ThreadMXBean mxBean = ManagementFactory.getThreadMXBean();
        
    }
}
