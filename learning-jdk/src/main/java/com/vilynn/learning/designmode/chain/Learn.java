package com.vilynn.learning.designmode.chain;

import javax.xml.ws.Response;

/**
 * Created by Weilin on 2018/7/2.
 */
public class Learn implements Interceptor {
    @Override
    public Response execute(InterceptorExecution execution) {
        System.out.println("好好学习 天天向上");
        return execution.execute();
    }
}
