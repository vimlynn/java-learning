package com.vilynn.learning.designmode.chain;

/**
 * Created by wangweilin on 2018/11/26.
 */
public interface Channel {
    Result send();
    void setChannel(Channel channel);
}
