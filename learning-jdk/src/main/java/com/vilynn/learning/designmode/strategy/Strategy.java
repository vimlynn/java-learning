package com.vilynn.learning.designmode.strategy;

/**
 * @author weilin.wang
 */
public interface Strategy {
    void strategyInterface();
}
