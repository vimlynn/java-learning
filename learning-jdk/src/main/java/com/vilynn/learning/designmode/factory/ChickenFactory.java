package com.vilynn.learning.designmode.factory;

/**
 * Created by wangweilin on 2019/10/23.
 */
public class ChickenFactory implements Factory {

    @Override
    public Animal create() {
        return new Chicken();
    }
}
