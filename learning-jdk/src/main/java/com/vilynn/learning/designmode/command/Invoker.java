package com.vilynn.learning.designmode.command;

import lombok.Data;

/**
 * Created by wangweilin on 2019/10/25.
 */
@Data
public class Invoker {
    private Command command;

    public Invoker(Command command) {
        this.command = command;
    }

    public void action(){
        command.execute();
    }
}
