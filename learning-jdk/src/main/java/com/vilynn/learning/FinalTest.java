package com.vilynn.learning;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author weilin.wang
 * @version v1.0
 * @date 2018/9/18
 * @see
 * @since 1.0
 */
public class FinalTest {
    public static void main(String[] args) {
        Integer i = 6;
        int k = 6;
        Integer j = new Integer(6);

        String a = "hello2";

        final String b = "hello";
        String c = b + 2;

        String d = "hello";
        final String e = d + 2;

        String f = "hello" + 2;
        final String h = new String("hello") + 2;

        System.out.println((a == c));
        System.out.println((a == e));
        System.out.println((a == f));
        System.out.println((a == h));

//        Computer computer = new Computer("123");
//        System.out.println(computer.getNo());
//        computer.change(computer);
//        System.out.println(computer.getNo());
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class Computer{
        private String no;
        void change(Computer computer){
            computer.setNo("456");
            computer = new Computer();
            System.out.println(computer.no);
        }

    }
}
