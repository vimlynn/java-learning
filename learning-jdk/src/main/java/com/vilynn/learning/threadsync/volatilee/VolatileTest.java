package com.vilynn.learning.threadsync.volatilee;

import lombok.Data;

/**
 * Created by wangweilin on 2018/10/17.
 */
@Data
public class VolatileTest {
    private int num;

    public static void main(String[] args) {
        VolatileTest volatileTest = new VolatileTest();
        for (int i = 0; i < 2; i++) {
            new Thread(() -> {
                for (int j = 0; j < 10; j++) {
                    volatileTest.setNum(volatileTest.getNum()+1);
                }
            }).start();
        }
        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(volatileTest.getNum());
    }
}
