package com.vilynn.learning.threadsync;

import lombok.Data;

/**
 * Created by wangweilin on 2018/9/21.
 */
@Data
public class Product {
    private Consumer consumer;
    private String something;

    public Product(Consumer consumer) {
        this.consumer = consumer;
    }

    public void producer(){
        synchronized (consumer) {
            something = "东西生产好了～～";
            consumer.notify();
            System.out.println("end notify....");
        }
    }
}
