package com.vilynn.learning.threadsync;

/**
 * Created by wangweilin on 2018/9/21.
 */
public class Consumer {
    public void consume(Product product){
        if(product != null){
            synchronized (this) {
                if(product.getSomething() == null){
                    try {
                        wait();
                        System.out.println("end wait......");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("消费:" + product.getSomething());
                product.setSomething(null);
            }
        }
    }
}
