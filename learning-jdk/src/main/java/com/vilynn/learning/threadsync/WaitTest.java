package com.vilynn.learning.threadsync;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by wangweilin on 2018/9/21.
 */
public class WaitTest {
    public static void main(String[] args) throws InterruptedException, ExecutionException {

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        Consumer consumer = new Consumer();
        Product product = new Product(consumer);

        new Thread(() -> {
            product.producer();

        }).start();

        Thread.sleep(1000L);
        System.out.println("休眠结束～～");

        new Thread(() -> {
            consumer.consume(product);
        }).start();

//        executorService.submit(() -> {
//            consumer.consume(product);
//        }).get();
//
//        Thread.sleep(1000L);
//        System.out.println("休眠结束～～");
//        executorService.submit(() -> {
//            product.producer();
//        }).get();


    }
}
