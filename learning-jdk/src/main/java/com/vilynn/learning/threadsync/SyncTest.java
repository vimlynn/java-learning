package com.vilynn.learning.threadsync;

/**
 * Created by wangweilin on 2019/1/25.
 */
public class SyncTest {
    private Object lock = new Object();
    private void f(){
        synchronized (lock){
            lock = null;
        }
    }

    public static void main(String[] args) {
        SyncTest app = new SyncTest();
        for (int i = 0; i < 2; i++) {
            new Thread(() -> app.f()).start();
        }
    }
}
