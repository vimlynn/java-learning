package com.vilynn.learning.blockingqueue;

import com.alibaba.fastjson.JSON;
import com.vilynn.learning.pojo.FunObjectFactory;

/**
 * @author weilin.wang
 */
public class QueueTest2 {
    public static void main(String[] args) throws InterruptedException {

        final MyBlockingQueue queue = new MyBlockingQueue(2);
        queue.add(FunObjectFactory.getBean());
        queue.add(FunObjectFactory.getBean());
        boolean offer = queue.offer(FunObjectFactory.getBean());
        System.out.println(offer);


        System.out.println("......");


        new Thread(() -> {
            try {
                Thread.sleep(2*1000);
                Object res = queue.poll(1);
                System.out.println(JSON.toJSON(res));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();


        queue.put(FunObjectFactory.getBean());
        System.out.println("end end end ");

        System.out.println("items:" + JSON.toJSON(queue.getItems()));

    }
}
