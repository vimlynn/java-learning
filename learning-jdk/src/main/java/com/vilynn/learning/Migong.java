package com.vilynn.learning;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author weilin.wang
 * @version v1.0
 * @date 2018/9/13
 * @see
 * @since 1.0
 */
public class Migong {

    public static void main(String[] args) {
        Set<String> set = initPoint();
        int[][] arr = init(set);
        print(arr);
        findLine(arr);
    }

    private static Set<String> initPoint() {
        return new HashSet<>(Arrays.asList("0,1", "0,5", "0,6",
                "1,2", "1,3",
                "2,0", "2,3", "2,5",
                "3,2", "3,5",
                "4,1", "4,4", "4,5",
                "5,3",
                "6,1", "6,3", "6,5"));
    }

    private static void print(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void findLine(int[][] arr) {
    }

    private static int[][] init(Set<String> set) {
        int[][] arr = new int[7][7];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if(set.contains(String.format("%d,%d", i, j))){
                    arr[i][j] = 1;
                }
            }
        }
        return arr;
    }


}
