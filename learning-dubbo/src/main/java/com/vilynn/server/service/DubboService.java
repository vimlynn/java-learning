package com.vilynn.server.service;

/**
 * Created by weilin.wang on 2018/4/9
 */
public interface DubboService {

    Object get(Long id);

}