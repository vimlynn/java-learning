package com.vilynn.server.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.vilynn.server.dto.StoreInfo;
import com.vilynn.server.service.DubboService;

/**
 * Created by weilin.wang on 2018/4/9
 */
@Service
public class DubboServiceImpl implements DubboService {
    @Override
    public Object get(Long id) {
        return new StoreInfo("门店");
    }
}