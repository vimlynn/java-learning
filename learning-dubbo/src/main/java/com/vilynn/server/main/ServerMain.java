package com.vilynn.server.main;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by weilin.wang on 2018/4/9
 */
@SpringBootApplication
@ComponentScan("com.vilynn.server")
@DubboComponentScan("com.lynn.server.service")
public class ServerMain {
    public static void main(String[] args) {
        SpringApplication.run(ServerMain.class, args);
    }
}