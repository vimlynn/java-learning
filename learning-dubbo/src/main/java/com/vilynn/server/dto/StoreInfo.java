package com.vilynn.server.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by weilin.wang on 2018/4/9
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreInfo implements Serializable {
    private String name;
}