package com.vilynn.socket.main;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by weilin.wang on 2018/4/10
 */
@SpringBootApplication
@ComponentScan("com.vilynn.socket")
@DubboComponentScan("com.lynn.api")
public class ApiMain {
    public static void main(String[] args) {
        SpringApplication.run(ApiMain.class, args);
    }
}
