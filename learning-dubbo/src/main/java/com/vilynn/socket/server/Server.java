package com.vilynn.socket.server;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by weilin.wang on 2018/4/12
 */
public class Server {
    public static void main(String[] args) throws IOException, InterruptedException {
        ServerSocket server = new ServerSocket(1000);

        while (true) {

            Socket socket = server.accept();

            InputStream inputStream = socket.getInputStream();

            byte[] bytes = new byte[1024];
            while (inputStream.read() != -1) {
                inputStream.read(bytes);
            }

            Thread.sleep(10000);
            socket.close();
            System.out.println(new String(bytes, "UTF-8"));
        }

    }
}
