package com.vilynn.socket.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * Created by weilin.wang on 2018/4/12
 */
public class Server2 {
    public static void main(String[] args) throws IOException, InterruptedException {
        ServerSocketChannel open = ServerSocketChannel.open();
//        open.configureBlocking(false);
        ServerSocket server = open.socket();
        server.bind(new InetSocketAddress(1000), 150);

        while (true) {

            SocketChannel sc = open.accept();


            ByteBuffer buffer = ByteBuffer.allocate(1024);
            sc.read(buffer);

            System.out.println(new String(buffer.array(), "UTF-8"));


            sc.close();
        }

    }
}
