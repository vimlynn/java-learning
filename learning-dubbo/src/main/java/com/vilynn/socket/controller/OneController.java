package com.vilynn.socket.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.vilynn.server.service.DubboService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by weilin.wang on 2018/4/10
 */
@Controller
public class OneController {

    @Reference
    private DubboService dubboService;
//    @Reference(registry = "registryConfigOrder")
//    private OrderQueryFacade orderQueryFacade;

    @RequestMapping("/get")
    @ResponseBody
    public Object get(){
        return dubboService.get(1L);
    }

    @RequestMapping("/order")
    @ResponseBody
    public Object order(){
//        return orderQueryFacade.getBaseOrder(1L, 1L);
        return null;
    }
}
