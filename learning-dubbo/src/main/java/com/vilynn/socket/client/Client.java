package com.vilynn.socket.client;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by weilin.wang on 2018/4/12
 */
public class Client {
    public static void main(String[] args) throws IOException, InterruptedException {
        Socket socket = new Socket("localhost", 1000);

        OutputStream outputStream = socket.getOutputStream();

        byte[] bytes = "娃儿啊".getBytes("UTF-8");
        outputStream.write(bytes);

        byte[] bytes2 = "娃儿啊2".getBytes("UTF-8");
        outputStream.write(bytes2);

//        Thread.sleep(1000);
//        outputStream.close();
        Thread.sleep(10000);
        socket.close();
        System.out.println(new String(bytes));
    }
}
