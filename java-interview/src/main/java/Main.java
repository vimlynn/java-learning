/**
 * Main
 *
 * @author Weilin Wang
 * @since 2020/12/1
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(new Inner().name="wwl");
    }

    @FunctionalInterface
    private interface Fun{
        void act(String a);
    }

    private static class Inner{
        String name;
    }

    public void method(Fun fun, String x){
        fun.act(x);
        int i = new Inner().hashCode();
        method((a) -> {
            System.out.println(a);
        }, x);
    }
}
