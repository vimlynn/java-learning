package com.vilynn.interview.leetcode;

import com.alibaba.fastjson.JSON;

public class Solution566 {

    public static int[][] matrixReshape(int[][] nums, int r, int c) {
        int[][] res = new int[r][];
        int x = 0;
        int y = 0;
        for (int i = 0; i < r; i++) {
            res[i] = new int[c];
            for (int j = 0; j < c; j++) {
                if (x >= nums.length) {
                    return nums;
                }
                res[i][j] = nums[x][y];
                y++;
                if (y >= nums[x].length) {
                    x++;
                    y = 0;
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
//        int[][] nums = new int[][]{{1,2},{3,4}};
        int[][] nums = new int[][]{{1,2,-1},{3,4}};

        System.out.println(JSON.toJSONString(nums));
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums[i].length; j++) {
                System.out.println(i + "," + j + " = " + nums[i][j]);
            }
        }
        int[][] res = matrixReshape(nums, 4, 1);
        System.out.println(JSON.toJSONString(res));
    }
}