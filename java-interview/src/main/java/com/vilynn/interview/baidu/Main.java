package com.vilynn.interview.baidu;

import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        String s = "1213ewrwrw45621wee";
        System.out.println(method(s));
    }

    public static String method(String str){
        if(str == null || str.isEmpty()){
            return str;
        }
        Set<Character> set = new HashSet<>();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (set.add(c)) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

}
