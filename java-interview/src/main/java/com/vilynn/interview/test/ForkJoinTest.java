package com.vilynn.interview.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

public class ForkJoinTest {

    private static final int areaNumber = 10;

    public static void main(String[] args) throws InterruptedException {
        long start = System.currentTimeMillis();

        forInsert();

        long end = System.currentTimeMillis();

        System.out.println("cos time : " + (end - start));

        long start2 = System.currentTimeMillis();

        forkJoinExe();

        long end2 = System.currentTimeMillis();

        System.out.println("cos time : " + (end2 - start2));

        Thread.sleep(20000L);
        System.out.println("结束");
    }

    public static void forInsert() {
        for (int i = 0; i < areaNumber; i++) {
            insert(i);
        }
    }

    public static void forkJoinExe() {
        ForkJoinPool forkJoinPool = new ForkJoinPool(areaNumber);
        List<MyForkJoinTask> list = new ArrayList<>();
        System.out.println("111111111111");
        for (int i = 0; i < areaNumber; i++) {
            MyForkJoinTask myForkJoinTask = new MyForkJoinTask(i);
            ForkJoinTask submit = forkJoinPool.submit(myForkJoinTask);
            System.out.println(submit.getRawResult());
        }
        System.out.println("2222222222222222");
//        for (MyForkJoinTask myForkJoinTask : list) {
//            Object rawResult = myForkJoinTask.getRawResult();
//            System.out.println(rawResult);
//        }

    }

    private static void insert(int areaNo) {
        // 插入座位实例
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(print() + String.format("区域【%d】插入座位", areaNo));
    }

    private static class MyForkJoinTask extends ForkJoinTask {

        private int areaNo;

        public MyForkJoinTask(int areaNo) {
            this.areaNo = areaNo;
        }

        @Override
        public Object getRawResult() {
            return "成功";
        }

        @Override
        protected void setRawResult(Object value) {

        }

        @Override
        protected boolean exec() {
            // 插入座位实例
            insert(areaNo);
            return false;
        }
    }

    private static String print(){
        Thread thread = Thread.currentThread();
        return String.format("线程id=%s,线程名称=%s,", thread.getId(), thread.getName());
    }

}
