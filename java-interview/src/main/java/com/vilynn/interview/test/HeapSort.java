package com.vilynn.interview.test;

public class HeapSort {

    public static void main(String[] args) {
        int[] a = new int[]{67, 65, 77, 38, 97, 3, 33, 49};
        heapSort(a, a.length);
    }


    static void heapSort(int a[], int n) {
        int i, j, h, k;
        int t;
        for (i = n / 2 - 1; i >= 0; i--) {
            while (2 * i + 1 < n) {
                j = 2 * i + 1;
                if (j + 1 < n) {
                    if (a[j] < a[j + 1]) {
                        j++;
                    }
                }
                if (a[i] < a[j]) {
                    t = a[i];
                    a[i] = a[j];
                    a[j] = t;
                    i = j;
                } else {
                    break;
                }
            }
        }
        System.out.println("原始堆");
        for (h = 0; h < n; h++) {
            System.out.print(" " + a[h]);
        }
    }

}
