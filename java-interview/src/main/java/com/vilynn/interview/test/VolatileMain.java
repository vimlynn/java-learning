package com.vilynn.interview.test;

public class VolatileMain {

    private static int i;

    public static void main(String[] args) {
        add();
    }

    private static void add() {
        Thread thread = new Thread(VolatileMain::run2);

        Thread thread1 = new Thread(VolatileMain::run2);

        thread.start();
        thread1.start();
    }

    private static void addOne() {
        Thread thread = new Thread(VolatileMain::run);

        Thread thread1 = new Thread(VolatileMain::run);

        thread.start();
        thread1.start();
    }

    private static void run() {
        i++;
        System.out.println(Thread.currentThread().getName() + ": " + i);
    }

    private static void run2() {
        for (int i1 = 0; i1 < 100; i1++) {
            i++;
            try {
                Thread.sleep(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getName() + ": " + i);
    }
}
