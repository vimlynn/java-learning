package com.vilynn.interview.xiaohongshu;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 链表求交集
 * 链表求并集
 */
public class LinkOperation {
    public static void main(String[] args) {
        Link link = new Link(1);
        link.next = new Link(2);
        link.next.next = new Link(3);
        link.next.next.next = new Link(4);
        link.next.next.next = new Link(5);

        Link link2 = new Link(8);
        link2.next = new Link(3);
        link2.next.next = new Link(1);

        Map<Integer, Link> linkMap = new LinkedHashMap<>();
        Link temp = link2;
        while (temp != null) {
            linkMap.put(temp.hashCode(), temp);
            temp = temp.next;
        }

        Link res = null;
        Link temp2 = link;
        while (temp2 != null) {
            if (!linkMap.containsKey(temp2.hashCode())) {
                linkMap.remove(temp2.hashCode());
            }
            temp2 = temp2.next;
        }
        System.out.println(JSON.toJSON(linkMap));
    }

    public static class Link {
        Object value;
        Link next;

        public Link(Object value) {
            this.value = value;
        }

        public Link(Object value, Link next) {
            this.value = value;
            this.next = next;
        }

        @Override
        public int hashCode(){
            return Integer.parseInt(value.toString());
        }
    }

}

