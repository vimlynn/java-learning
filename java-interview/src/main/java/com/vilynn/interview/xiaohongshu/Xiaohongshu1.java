package com.vilynn.interview.xiaohongshu;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author weilin
 * @since 2020/12/17
 */
public class Xiaohongshu1 {
    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<String>();
        strings.add("Hello, World!");
        strings.add("Welcome to online interview system of Acmcoder.");
        strings.add("This system is running Java 8.");

        for (String string : strings) {
            System.out.println(string);
        }

        String s;
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            s = in.nextLine();
            boolean b = judge(s);
            System.out.println(b ? "合法的" : "非法的");
        }
    }

    public static boolean judge(String s) {
        Map<Character, Integer> map = new LinkedHashMap<>();
        map.put('(', 0);
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            Integer count = map.get('(');
            if (c == '(') {
                map.put(c, count + 1);
            } else if (c == ')') {
                map.put('(', count - 1);
            } else {
                return false;
            }
        }
        return map.get('(') == 0;
    }
}
