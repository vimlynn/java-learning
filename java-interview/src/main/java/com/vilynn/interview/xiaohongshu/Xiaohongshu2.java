package com.vilynn.interview.xiaohongshu;

/**
 * weilin
 * 2020/12/12
 */
public class Xiaohongshu2 {
    static class Node {
        Node left;
        Node right;
        int value;

        public Node(int value) {
            this.value = value;
        }
    }

    public static void main(String[] args) {
        Node root = new Node(1);
        root.left = new Node(2);

        root.left.left = new Node(4);
        root.left.right = new Node(5);

        root.left.left.left = new Node(8);
        root.left.left.right = new Node(9);

        root.right = new Node(3);
        root.right.left = new Node(6);
        root.right.right = new Node(7);

        printTree(root);
    }

    public static void printTree(Node root){
        if (root != null) {
            System.out.println(root.value);
            printTree(root.left);
            printTree(root.right);

//            Node left = root.left;
//            while (left != null) {
//                System.out.println(left.value);
//                left = left.left;
//            }
//
//            left = root.left == null ? null : root.left.right;
//            while (left != null) {
//                System.out.println(left.value);
//                left = left.right;
//            }
//
//            printTree(root.right);
        }
    }
}
