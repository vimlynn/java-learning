package com.vilynn.interview.huawei.nowcoder;

import java.util.*;

/**
 * 华为机试考题1
 */
public class Exam1 {

    private static final Set<Character> set = new HashSet<>();

    static {
        String yuan_yin = "aeiouAEIOU";
        for (int i = 0; i < yuan_yin.length(); i++) {
            set.add(yuan_yin.charAt(i));
        }
    }

    public static int sub(String string){

        Stack<Character> stack = new Stack<>();
        int length = 0;

        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            if (set.contains(c)) {
                stack.add(c);
            } else {
                int size = stack.size();
                if (size > length) {
                    length = size;
                }
                stack.clear();
            }
        }
        return Integer.max(length, stack.size());
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            System.out.println(sub(line));
        }
    }

}
