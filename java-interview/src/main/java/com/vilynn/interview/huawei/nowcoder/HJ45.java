package com.vilynn.interview.huawei.nowcoder;

import java.util.*;
import java.util.stream.Collectors;

public class HJ45 {

    public static int pretty(String name) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < name.length(); i++) {
            char c = Character.toUpperCase(name.charAt(i));
            map.merge(c, 1, Integer::sum);
        }
        List<Character> collect = map.keySet().stream().collect(Collectors.toList());
        collect.sort(Comparator.comparing(k -> map.get(k)));
        int max = 0;
        int n = 26;
        for (int size = collect.size() - 1; size >= 0; size--) {
            Character c = collect.get(size);
            max += map.get(c) * n--;
        }
        return max;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            int line = Integer.parseInt(scanner.nextLine());
            for (int i = 0; i < line; i++) {
                String next = scanner.nextLine();
                System.out.println(pretty(next));
            }
        }
    }
}
