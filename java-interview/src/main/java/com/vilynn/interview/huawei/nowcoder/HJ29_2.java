package com.vilynn.interview.huawei.nowcoder;

import java.util.*;

public class HJ29_2 {

    private static final String string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static String entry(String key, String original){
        List<Character> keys = new ArrayList<>();
        for (int i = 0; i < key.length(); i++) {
            char c = Character.toUpperCase(key.charAt(i));
            if (!keys.contains(c)) {
                keys.add(c);
            }
        }
        String temp = string;
        StringBuilder sb = new StringBuilder();
        for (Character c : keys) {
            sb.append(c);
            temp = temp.replace(String.valueOf(c), "");
        }
        temp = sb.append(temp).toString();
        Map<Character, Character> map = new HashMap<>();
        for (int i = 0; i < temp.length(); i++) {
            map.put(string.charAt(i), temp.charAt(i));
        }

        StringBuilder entry = new StringBuilder();
        for (int i = 0; i < original.length(); i++) {
            char charAt = original.charAt(i);
            char c = Character.toUpperCase(charAt);
            if (map.get(c) != null) {
                if (Character.isLowerCase(charAt)) {
                    entry.append(Character.toLowerCase(map.get(c)));
                } else {
                    entry.append(map.get(c));
                }
            }else {
                entry.append(charAt);
            }
        }
        return entry.toString();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String key = scanner.nextLine();
            String original = scanner.nextLine();
            System.out.println(entry(key, original));
        }
    }

}
