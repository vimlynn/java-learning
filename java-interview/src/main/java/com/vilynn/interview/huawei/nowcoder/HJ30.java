package com.vilynn.interview.huawei.nowcoder;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class HJ30 {

    public static String mergeSort(String str1, String str2) {
        str1 = str1 + str2;
        List<Character> list1 = new ArrayList<>();
        List<Character> list2 = new ArrayList<>();
        for (int i = 0; i < str1.length(); i++) {
            if (i % 2 == 0) {
                list1.add(str1.charAt(i));
            } else {
                list2.add(str1.charAt(i));
            }
        }
        list1.sort(Comparator.comparing(Integer::valueOf));
        list2.sort(Comparator.comparing(Integer::valueOf));
        int size = Integer.max(list1.size(), list2.size());
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            if (i < list1.size()) {
                sb.append(list1.get(i));
            }
            if (i < list2.size()) {
                sb.append(list2.get(i));
            }
        }
        str1 = sb.toString();
        sb = new StringBuilder();
        for (int i = 0; i < str1.length(); i++) {
            char c = str1.charAt(i);
            if (isReverse(c)) {
                sb.append(reverse(c));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    private static char reverse(char c) {
        String binary = Integer.toBinaryString(Integer.valueOf(String.valueOf(c),16));
        while (binary.length() < 4) {
            binary = '0' + binary;
        }
        binary = new StringBuilder(binary).reverse().toString();
        String s = Integer.toHexString(Integer.valueOf(binary, 2));
        return Character.toUpperCase(s.charAt(0));
    }

    private static boolean isReverse(char c) {
        if (c >= '0' && c <= '9') {
            return true;
        }
        if (c >= 'A' && c <= 'F') {
            return true;
        }
        if (c >= 'a' && c <= 'f') {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String[] str1 = scanner.nextLine().split(" ");
            String res = mergeSort(str1[0], str1[1]);
            System.out.println(res);
        }
    }

}
