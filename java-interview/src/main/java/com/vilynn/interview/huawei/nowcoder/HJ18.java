package com.vilynn.interview.huawei.nowcoder;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HJ18 {

    private static final Map<Character, Integer> map = new HashMap<>();
    private static final Character[] keys = {'A','B','C','D','E','X','S'};
    static {
        for (Character key : keys) {
            map.put(key, 0);
        }
    }

    public static void count(String ip, String yan_ma){
        // 检验ip
        String[] splitIp = ip.split("\\.");
        boolean i = validIp(splitIp);
        if (i) {
            count(splitIp);
        }
        // 检验掩码
        String[] yan_mas = yan_ma.split("\\.");
        validYan_ma(yan_mas);
    }

    private static void count(String[] splitIp) {
        int anInt = Integer.parseInt(splitIp[0]);
        int secondInt = Integer.parseInt(splitIp[1]);

        if (anInt >= 1 && anInt <= 126) {
            if (anInt == 10) {
                map.put('S', map.get('S') + 1);
            } else {
                map.put('A', map.get('A') + 1);
            }
        } else if (anInt >= 128 && anInt <= 191) {
            if (anInt == 172 && (secondInt >= 16 && secondInt <= 31)) {
                map.put('S', map.get('S') + 1);
            } else {
                map.put('B', map.get('B') + 1);
            }
        } else if (anInt >= 192 && anInt <= 223) {
            if (anInt == 192 && secondInt == 168) {
                map.put('S', map.get('S') + 1);
            } else {
                map.put('C', map.get('C') + 1);
            }
        } else if (anInt >= 224 && anInt <= 239) {
            map.put('D', map.get('D') + 1);
        } else if (anInt >= 240 && anInt <= 255) {
            map.put('E', map.get('E') + 1);
        }
    }

    private static void validYan_ma(String[] splitYan_ma) {
        StringBuilder sb = new StringBuilder();
        for (String s : splitYan_ma) {
            if ("".equals(s) || Integer.parseInt(s) < 0 || Integer.parseInt(s) > 255) {
                map.put('X', map.get('X') + 1);
                return;
            }
            StringBuilder binary = new StringBuilder(Integer.toBinaryString(Integer.parseInt(s)));
            while (binary.length() < 8) {
                binary.insert(0, '0');
            }
            sb.append(binary);
        }
        String yan_ma = sb.toString();
        int i = yan_ma.lastIndexOf('1');
        if (yan_ma.substring(0, i).indexOf('0') > -1) {
            map.put('X', map.get('X') + 1);
        }
    }

    private static boolean validIp(String[] splitIp) {
        for (String s : splitIp) {
            if ("".equals(s) || Integer.parseInt(s) < 0 || Integer.parseInt(s) > 255) {
                map.put('X', map.get('X') + 1);
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            if ("\n".equals(line)) {
                break;
            }
            String[] split = line.split("~");
            count(split[0], split[1]);
        }
            for (Character key : keys) {
                System.out.print(map.get(key) + " ");
            }
            System.out.println();
    }

}
