package com.vilynn.interview.huawei.nowcoder;

import java.util.*;

/**
 * 华为机试考题3
 *
 * 10
 * [1, 1, 1, 1, -4, 1]
 */
public class Exam3 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int seatNum = sc.nextInt();
        sc.nextLine();
        String seatOrLeaveLine = sc.nextLine();
        String[] c = seatOrLeaveLine.substring(1,seatOrLeaveLine.length()-1).split(", ");
        int[] seatOrLeave = new int[c.length];
        for (int i = 0; i < c.length; i++) {
            seatOrLeave[i] = Integer.parseInt(c[i]);
        }

        Exam3 socialDistance = new Exam3();
        int ans = socialDistance. conferenceSeatDistance(seatNum, seatOrLeave);
        System.out.println(ans);
    }

    /**
     * 计算最后进来的人，坐的位置
     * @param seatNum 会议室座位总数
     * @param seatOrLeave 员工的进出顺序
     * @return 最后进来的人，坐的位置
     */
    public int conferenceSeatDistance(int seatNum, int[] seatOrLeave) {
        Map<Integer, Boolean> map = new HashMap<>();
        for (int i = 0; i < seatNum; i++) {
            map.put(i, false);
        }

        for (int i = 0; i < seatOrLeave.length; i++) {
            int people = seatOrLeave[i];
            if (people < 0) {
                map.put(-people, false);
                continue;
            }
            // 第一个人坐0号
            if (i == 0) {
                map.put(0, true);
                continue;
            }
            // 第二个人必然坐最后一个位置，因为第一个人不会离开
            if (i == 1) {
                map.put(seatNum-1, true);
                continue;
            }

            // 开始选座位
            List<String> posList = new ArrayList<>();
            for (int j = 0; j < seatNum; j++) {
                if (map.get(j)) {
                    // 有人
                    continue;
                }
                int distance = calculateDistance(map, j, seatNum);
                posList.add(j + ":" + distance);
            }
            if (posList.size() == 0) {
                return -1;
            }

            posList.sort((o1, o2) -> {
                String[] split1 = o1.split(":");
                String[] split2 = o2.split(":");
                if (Integer.parseInt(split1[1]) < Integer.parseInt(split2[1])) {
                    return 1;
                }
                if (Integer.parseInt(split1[1]) > Integer.parseInt(split2[1])) {
                    return -1;
                }
                return 0;
            });
            // 坐下
            int pos = Integer.parseInt(posList.get(0).split(":")[0]);
            map.put(pos, true);
            if (i == seatOrLeave.length-1) {
                return pos;
            }
        }
        return -1;
    }

    private int calculateDistance(Map<Integer, Boolean> map, int j, int seatNum) {
        // j位置和别人的距离是多少呢？看前后进行计算
        int a = 0;
        // 往前找
        for (int before = j-1; before >= 0; before--) {
            if (map.get(before)) {
                a = j - before - 1;
                break;
            }
        }
        // 往后找
        int b = 0;
        for (int after = j+1; after < seatNum; after++) {
            if (map.get(after)) {
                b = after - j - 1;
                break;
            }
        }
        return Integer.min(a, b);
    }
}