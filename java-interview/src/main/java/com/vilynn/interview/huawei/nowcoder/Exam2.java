package com.vilynn.interview.huawei.nowcoder;

import java.util.*;

/**
 * 华为机试考题2
 */
public class Exam2 {

    private static final Map<String, Integer> map = new HashMap<>();

    static {
        String weightStr = "abcdefghijklmnopqrstuvwxyz";
        for (int i = 0; i < weightStr.length(); i++) {
            char c = weightStr.charAt(i);
            map.put(String.valueOf(c), i);
        }
    }

    public static String reverse(String string) {
        List<String> list = new ArrayList<>();

        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            list.add(c + ":" + i);

        }
        list.sort(Comparator.comparing(a -> map.get(a.split(":")[0])));

        return cal(string, map, list);
    }

    private static String cal(String string, Map<String, Integer> map, List<String> list) {
        for (int i = 0; i < list.size(); i++) {
            String[] split = list.get(i).split(":");
            String currentC = split[0];
            int currentIndex = Integer.parseInt(split[1]);

            for (int j = 0; j < currentIndex; j++) {
                char c = string.charAt(j);
                if (!currentC.equals(c) && map.get(String.valueOf(c)) > map.get(currentC)) {
                    // 找到最后位置的这个字符
                    int maxIndex = string.lastIndexOf(currentC);

                    StringBuilder sb = new StringBuilder();
                    for (int i1 = 0; i1 < string.length(); i1++) {
                        if (i1 == j) {
                            sb.append(string.charAt(maxIndex));
                        } else if (i1 == maxIndex) {
                            sb.append(string.charAt(j));
                        } else {
                            sb.append(string.charAt(i1));
                        }
                    }
                    return sb.toString();
                }
            }
        }
        return string;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            System.out.println(reverse(line));
        }
    }

}
