package com.vilynn.interview.huawei.nowcoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class HJ50 {

    private static final char[] splits = new char[]{'+','-','*','/','{','[','(',')',']','}'};

    public static int calculate(String strExpression){
        // 数字栈
        Stack<Integer> numberStack = new Stack();
        // 符号栈
        Stack<Character> signStack = new Stack();

        for (int i = 0; i < strExpression.length(); i++) {
            char c = strExpression.charAt(i);
            if (c >= 48 && c <= 57) {
                // 是数字，入数字栈
                Integer integer = Integer.valueOf(c + "");
                if (signStack.empty()) {
                    numberStack.add(integer);
                    continue;
                }
                Character pop = signStack.pop();
                if (pop == '*' || pop == '/') {
                    Integer pop2 = numberStack.pop();
                    int result = calculate(pop2, integer, pop);
                    numberStack.add(result);
                } else {
                    signStack.add(pop);
                    numberStack.add(integer);
                }
            } else {
                if (c == '(' || c == '[' || c == '{') {
                    signStack.add(c);
                    // 如果括号后面是负数，插入0
                    char nextC = strExpression.charAt(i + 1);
                    if (nextC < 48 || nextC > 57) {
                        numberStack.add(0);
                    }
                    continue;
                }
                if (c == ')' || c == ']' || c == '}') {
                    // 一直弹出前括号为止
                    List<String> list = new ArrayList<>();
                    int integer = numberStack.pop();
                    list.add(String.valueOf(integer));
                    Character pop = signStack.pop();
                    while (true) {
                        if (pop == '(' || pop == '[' || pop == '{') {
                            break;
                        }
                        list.add(0, pop+"");
                        list.add(0, String.valueOf(numberStack.pop()));
                        pop = signStack.pop();
                    }
//                    int anInt = Integer.parseInt(String.valueOf(list.get(0)));
//                    for (int k = 1; k < list.size();) {
//                        anInt = calculate(anInt, Integer.parseInt(String.valueOf(list.get(k+1))), list.get(k).toCharArray()[0]);
//                        k+=2;
//                    }
                    StringBuilder sb = new StringBuilder();
                    for (int k = 0; k < list.size(); k++) {
                        sb.append(list.get(k));
                    }
                    // 两位数就有bug，如何解决?
                    int anInt = calculate(sb.toString());
                    numberStack.add(anInt);
                    continue;
                }
                if (signStack.empty()) {
                    signStack.add(c);
                    continue;
                }
                // 取出栈顶运算符
                Character pop = signStack.pop();
                if (pop == '(' || pop == '[' || pop == '{') {
                    signStack.add(pop);
                    signStack.add(c);
                    continue;
                }
                if ((c == '*' || c == '/') && (pop == '+' || pop == '-')) {
                    // 入符号栈
                    signStack.add(pop);
                } else {
                    Integer pop1 = numberStack.pop();
                    Integer pop2 = numberStack.pop();
                    int result = calculate(pop2, pop1, pop);
                    numberStack.add(result);
                }
                signStack.add(c);
            }
        }
        Integer pop1 = numberStack.pop();
        while (signStack.size() > 0) {
            Character pop = signStack.pop();
            Integer pop2 = numberStack.pop();
            pop1 = calculate(pop2, pop1, pop);
        }
        return pop1;
    }

    private static int calculate(Integer pop2, Integer pop1, Character pop) {
        switch (pop) {
            case '+' : return pop2 + pop1;
            case '-' : return pop2 - pop1;
            case '*' : return pop2 * pop1;
            case '/' : return pop2 / pop1;
        }
        return 0;
    }

    public static void main(String[] args) {
//        String strExpression = "2*4/(2*1+1+(2-3*1))";
//        String strExpression = "3+2*{1+2*[-4/(8-6)+7]}";
//        String strExpression = "3*5+8-0*3-6+0+0";
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String s = scanner.nextLine();
            int calculate = calculate(s);
            System.out.println(calculate);
        }
    }
}
