package com.vilynn.interview.huawei.nowcoder;

import java.util.ArrayList;
import java.util.List;

/**
 * 华为面试题2
 *
 * 字符串划分片段
 */
public class Interview2_2 {

    public static void main(String[] args) {
//        String s = "abcdefaacnmjlp";
        String s = "qweqrtrnyiuusfzdfhjxcvcvbvnmz";
//        String s = "ababcbacadefegdehijhklij";
        int[] arrays = sp(s);
        for (int i = 0; i < arrays.length; i++) {
            System.out.print(arrays[i] + ",");
        }
    }

    private static int[] sp(String s) {

        List<String> stringList = sub(s);

        int size = stringList.size();
        int[] arrays = new int[size];
        for (int i = 0; i < arrays.length; i++) {
            arrays[i] = stringList.get(i).length();
            System.out.print(stringList.get(i) + ",");
        }
        System.out.println("-----------------------");
        return arrays;
    }

    private static List<String> sub(String string) {
        List<String> stringList = new ArrayList<>();
        String temp = string;
        for (int i = 0; i < string.length();) {

            int index = 0;
            for (int j = 0; j <= index; j++) {
                char c = temp.charAt(j);
                int lastIndexOf = temp.lastIndexOf(c);
                if (lastIndexOf > index) {
                    index = lastIndexOf;
                }
            }

            stringList.add(temp.substring(0, index+1));
            temp = temp.substring(index+1);
            i += index+1;
        }
        return stringList;
    }

}
