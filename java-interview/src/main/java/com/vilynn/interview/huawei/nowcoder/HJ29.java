package com.vilynn.interview.huawei.nowcoder;

import java.util.*;
import java.util.stream.Collectors;

public class HJ29 {

    private static final Map<Character, Character> map = new LinkedHashMap<>();

    static {
        map.put('A', 'A');
        map.put('B', 'B');
        map.put('C', 'C');
        map.put('D', 'D');
        map.put('E', 'E');
        map.put('F', 'F');
        map.put('G', 'G');
        map.put('H', 'H');
        map.put('I', 'I');
        map.put('J', 'J');
        map.put('K', 'K');
        map.put('L', 'L');
        map.put('M', 'M');
        map.put('N', 'N');
        map.put('O', 'O');
        map.put('P', 'P');
        map.put('Q', 'Q');
        map.put('R', 'R');
        map.put('S', 'S');
        map.put('T', 'T');
        map.put('U', 'U');
        map.put('V', 'V');
        map.put('W', 'W');
        map.put('X', 'X');
        map.put('Y', 'Y');
        map.put('Z', 'Z');
    }

    public static String entry(String key, String original) {
        List<Character> set = new ArrayList<>();
        for (int i = 0; i < key.length(); i++) {
            char c = Character.toUpperCase(key.charAt(i));
            if (!set.contains(c)) {
                set.add(c);
                map.put(c, null);
            }
        }
        List<Character> keyList = map.keySet().stream().collect(Collectors.toList());
        int que_kou = 0;
        for (int size = keyList.size()-1; size >= 0; size--) {
            Character keyTemp = keyList.get(size);
            if (map.get(keyTemp) != null) {
                continue;
            }
            ++que_kou;
            for (int i = size-que_kou; i >= 0; i--) {
                Character character = keyList.get(i);
                if (character == null) {
                    ++que_kou;
                } else {
                    map.put(keyTemp, map.get(i));
                }
            }
        }
        for (int i = 0; i < set.size(); i++) {
            map.put(keyList.get(i), set.get(i));
        }

        StringBuilder entry = new StringBuilder();
        for (int i = 0; i < original.length(); i++) {
            char charAt = original.charAt(i);
            char c = Character.toUpperCase(charAt);
            if (map.get(c) != null) {
                if (Character.isLowerCase(charAt)) {
                    entry.append(Character.toLowerCase(map.get(c)));
                } else {
                    entry.append(map.get(c));
                }
            }else {
                entry.append(charAt);
            }
        }
        return entry.toString();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String key = scanner.nextLine();
        String original = scanner.nextLine();
        System.out.println(entry(key, original));
    }
}
