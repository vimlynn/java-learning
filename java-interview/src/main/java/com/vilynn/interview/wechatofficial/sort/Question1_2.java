package com.vilynn.interview.wechatofficial.sort;

/**
 * Question1_2
 *
 * @author Weilin Wang
 * @since 2020/11/18
 */
public class Question1_2 {
    private static volatile int i = 0;

    public static void main(String[] args) {
        new Thread(new MyThread(0)).start();
        new Thread(new MyThread(1)).start();
    }

    private static class MyThread implements Runnable {
        private int order;

        public MyThread(int order) {
            this.order = order;
        }


        @Override
        public void run() {
            Thread t = Thread.currentThread();
            while (i < 10) {
                if (i%2 == order) {
                    System.out.println(String.format("id:%s name:%s 输出%s", t.getId(), t.getName(), i));
                    i++;
                }
            }
        }
    }
}
