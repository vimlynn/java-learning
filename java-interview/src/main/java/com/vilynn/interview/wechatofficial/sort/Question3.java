package com.vilynn.interview.wechatofficial.sort;

import java.util.ArrayList;
import java.util.List;

/**
 * Question3
 *
 * @author Weilin Wang
 * @since 2020/11/18
 */
public class Question3 {

    private static final List<Cheese> cheeses = new ArrayList<>();
    private static final List<Milk> milks = new ArrayList<>();
    private static final List<Culture> cultures = new ArrayList<>();




    // 1奶酪 = 2milk+1culture
    private static class Cheese {

    }

    // 牛奶
    private static class Milk {

    }

    // 发酵剂
    private static class Culture {

    }
}
