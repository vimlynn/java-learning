package com.vilynn.interview.bilibili;

import java.util.*;

/**
 * 小A最多会新认识的多少人
 * <p>
 * 描述
 * 小A参加了一个n人的活动，每个人都有一个唯一编号i(i>=0 & i<n)，其中m对相互认识，在活动中两个人可以通过互相都认识的一个人介绍认识。
 * 现在问活动结束后，小A最多会认识多少人？
 * <p>
 * 输入描述：
 * 第一行聚会的人数：n（n>=3 & n<10000）；
 * 第二行小A的编号: ai（ai >= 0 & ai < n)；
 * 第三互相认识的数目: m（m>=1 & m
 * < n(n-1)/2）；
 * 第4到m+3行为互相认识的对，以','分割的编号。
 * 输出描述：
 * 输出小A最多会新认识的多少人？
 */
public class NewFriend {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int ai = scanner.nextInt();
        int m = scanner.nextInt();
        String s = scanner.nextLine();

        String[] abs = new String[m];
        for (int i = 0; i < m; i++) {
            abs[i] = scanner.nextLine();
        }
        System.out.println(friend(n, ai, m, abs));
    }

    private static int friend(int n, int ai, int m, String[] abs) {

        Map<Integer, HashSet<Integer>> map = new LinkedHashMap<>();
        for (String ab : abs) {
            String[] split = ab.split(",");
            int a = Integer.parseInt(split[0]);
            int b = Integer.parseInt(split[1]);
            map.computeIfAbsent(a, o -> new HashSet<>()).add(b);
            map.computeIfAbsent(b, o -> new HashSet<>()).add(a);
        }

        HashSet<Integer> set = new HashSet<>();
        int size = map.get(ai).size();

        Queue<Integer> queue = new LinkedList<>();
        queue.add(ai);
        while (!queue.isEmpty()){
            HashSet<Integer> integers = map.get(queue.poll());
            for (Integer integer : integers) {
                if (!set.contains(integer)) {
                    set.add(integer);
                    queue.offer(integer);
                }
            }
        }
        return set.size() - size - 1;
    }

    private static int newSet(Map<Integer, HashSet<Integer>> map, HashSet<Integer> set) {

        int oldNum = set.size();
        for (Map.Entry<Integer, HashSet<Integer>> entry : map.entrySet()) {
            if (set.contains(entry.getKey())) {
                set.addAll(entry.getValue());
            }
            for (Integer integer : entry.getValue()) {
                if (set.contains(integer)) {
                    set.add(entry.getKey());
                }
            }
        }
        int newNum = set.size();
        return oldNum == newNum ? newNum : newSet(map, set);
    }

    private static class Node {
        private boolean isAi;
        private boolean isNew;
        private int val;
        private Node next;

        public Node(boolean isAi, boolean isNew, int val, Node next) {
            this.isAi = isAi;
            this.isNew = isNew;
            this.val = val;
            this.next = next;
        }
    }
}
