package com.vilynn.interview.nowcoder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 设计LRU缓存结构，该结构在构造时确定大小，假设大小为K，并有如下两个功能
 * set(key, value)：将记录(key, value)插入该结构
 * get(key)：返回key对应的value值
 * [要求]
 * set和get方法的时间复杂度为O(1)
 * 某个key的set或get操作一旦发生，认为这个key的记录成了最常使用的。
 * 当缓存的大小超过K时，移除最不经常使用的记录，即set或get最久远的。
 * 若opt=1，接下来两个整数x, y，表示set(x, y)
 * 若opt=2，接下来一个整数x，表示get(x)，若x未出现过或已被移除，则返回-1
 * 对于每个操作2，输出一个答案
 */
public class Nc93 {
    private Map<Integer, Node> map = new HashMap<>();
    private Node head = new Node(-1, -1);
    private Node tail = new Node(-1, -1);

    /**
     * lru design
     *
     * @param operators int整型二维数组 the ops
     * @param k         int整型 the k
     * @return int整型一维数组
     */
    public int[] LRU(int[][] operators, int k) {
        // write code here
        int[] res = new int[operators.length];
        int length = 0;
        for (int[] operator : operators) {
            int opt = operator[0];
            if (opt == 1) {
                res[length++] = get(operator[1]);
            } else if (opt == 2) {
                set(operator[1], operator[2]);
            }
        }
        return Arrays.copyOf(res, length - 1);
    }

    private int get(int x) {
        Node node = map.get(x);
        if (node == null) {
            return -1;
        }
        moveToHead(node);
        return node.val;
    }

    private void moveToHead(Node node) {
    }

    private void set(int x, int y) {
        Node node = new Node(x, y);
        map.put(x, node);
        addToHead(node);
    }

    private void addToHead(Node node) {
        
    }

    private static class Node {
        private int key, val;
        private Node prev, next;

        public Node(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }
}
